package edu.wmi.dpri.przychodnia.android.edit.user.data;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.wmi.dpri.przychodnia.android.R;

import static com.google.common.base.Preconditions.checkNotNull;

public class EditUserDataFragment extends Fragment implements EditUserDataContractIf.ViewIf {

    private EditUserDataContractIf.PresenterIf mEditUserDataPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_user_data_frag, container, false);
    }

    @Override
    public void setPresenter(@NonNull EditUserDataContractIf.PresenterIf presenter) {
        mEditUserDataPresenter = checkNotNull(presenter, "presenter can not be null");
    }
}
