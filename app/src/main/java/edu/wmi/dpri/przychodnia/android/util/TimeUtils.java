package edu.wmi.dpri.przychodnia.android.util;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeUtils {
    private static final long MONTH_IN_MILLIS = 2629743000L;
    private static final String TIME_24_HOURS_PATTERN = "([01]?[0-9]|2[0-3]):([0-5][0-9])";

    public static long now() {
        return System.currentTimeMillis();
    }

    public static long xMonthsLater(int x) {
        return now() + x * MONTH_IN_MILLIS;
    }

    public static long xMonthsEarlier(int x) {
        return now() - x * MONTH_IN_MILLIS;
    }

    public static Timepoint parseTime24Hours(String time) {
        Pattern time24HoursPattern = Pattern.compile(TIME_24_HOURS_PATTERN);
        Matcher matcher = time24HoursPattern.matcher(time);
        if (matcher.find()) {
            int hour = Integer.parseInt(matcher.group(1));
            int minute = Integer.parseInt(matcher.group(2));

            return new Timepoint(hour, minute);
        }
        throw new IllegalArgumentException("time '" + time + "' does not match pattern 'H:mm'");
    }

    public static String timestampToPrettyDate(long timestamp) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(timestamp);
        DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.SHORT);
        formatter.setTimeZone(calendar.getTimeZone());

        return formatter.format(calendar.getTime());
    }
}
