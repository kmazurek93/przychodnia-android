package edu.wmi.dpri.przychodnia.android.service.visits;

import android.support.annotation.NonNull;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import edu.wmi.dpri.przychodnia.android.endpoint.VisitsEndpointIf;
import edu.wmi.dpri.przychodnia.android.model.data.calendar.CalendarEntity;
import edu.wmi.dpri.przychodnia.android.model.data.calendar.CalendarFilter;
import edu.wmi.dpri.przychodnia.android.model.data.visit.ChangeDateRequestBody;
import edu.wmi.dpri.przychodnia.android.model.data.visit.CreateVisitRequestBody;
import edu.wmi.dpri.przychodnia.android.model.data.visit.SimpleVisit;
import edu.wmi.dpri.przychodnia.android.model.data.visit.SimpleVisitsResponse;
import edu.wmi.dpri.przychodnia.android.model.data.visit.VisitDetails;
import edu.wmi.dpri.przychodnia.android.model.data.visit.VisitFilter;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

@Singleton
public class VisitsService implements VisitsServiceIf {

    private final VisitsEndpointIf mVisitsEndpoint;

    @Inject
    VisitsService(@NonNull VisitsEndpointIf visitsEndpoint) {
        mVisitsEndpoint = checkNotNull(visitsEndpoint, "visitsEndpoint can not be null");
    }

    @Override
    public void getSimpleVisits(@NonNull VisitFilter visitFilter,
                                @NonNull final GetSimpleVisitsCallbackIf callback) {
        checkNotNull(visitFilter, "visitFilter can not be null");
        checkNotNull(callback, "callback can not be null");

        mVisitsEndpoint.findMySimpleVisits(visitFilter).enqueue(new Callback<SimpleVisitsResponse>() {
            @Override
            public void onResponse(Call<SimpleVisitsResponse> call, Response<SimpleVisitsResponse> response) {
                onFindMySimpleVisitsResponse(response, callback);
            }

            @Override
            public void onFailure(Call<SimpleVisitsResponse> call, Throwable t) {
                callback.onGetSimpleVisitsFailure();
            }
        });
    }

    @Override
    public void getDoctorCalendar(@NonNull CalendarFilter calendarFilter,
                                  @NonNull final GetDoctorCalendarCallbackIf callback) {
        checkNotNull(calendarFilter, "calendarFilter can not be null");
        checkNotNull(callback, "callback can not be null");

        mVisitsEndpoint.getDoctorCalendar(calendarFilter).enqueue(new Callback<Map<Date, List<CalendarEntity>>>() {
            @Override
            public void onResponse(Call<Map<Date, List<CalendarEntity>>> call, Response<Map<Date, List<CalendarEntity>>> response) {
                onGetDoctorCalendarResponse(response, callback);
            }

            @Override
            public void onFailure(Call<Map<Date, List<CalendarEntity>>> call, Throwable t) {
                callback.onGetDoctorCalendarFailure();
            }
        });
    }

    @Override
    public void createVisit(@NonNull CreateVisitRequestBody createVisitRequestBody,
                            @NonNull final CreateVisitCallbackIf callback) {
        checkNotNull(createVisitRequestBody, "createVisitRequestBody can not be null");
        checkNotNull(callback, "callback can not be null");

        mVisitsEndpoint.createVisit(createVisitRequestBody).enqueue(new Callback<SimpleVisit>() {
            @Override
            public void onResponse(Call<SimpleVisit> call, Response<SimpleVisit> response) {
                onCreateVisitResponse(response, callback);
            }

            @Override
            public void onFailure(Call<SimpleVisit> call, Throwable t) {
                callback.onCreateVisitFailure();
            }
        });
    }

    @Override
    public void changeVisitDate(@NonNull ChangeDateRequestBody changeDateRequestBody, @NonNull final ChangeVisitDateCallbackIf callback) {
        checkNotNull(changeDateRequestBody, "changeDateRequestBody can not be null");
        checkNotNull(callback, "callback can not be null");

        mVisitsEndpoint.changeVisitDate(changeDateRequestBody).enqueue(new Callback<SimpleVisit>() {
            @Override
            public void onResponse(Call<SimpleVisit> call, Response<SimpleVisit> response) {
                onChangeVisitDateResponse(response, callback);
            }

            @Override
            public void onFailure(Call<SimpleVisit> call, Throwable t) {
                callback.onChangeVisitDateFailure();
            }
        });
    }

    @Override
    public void deleteVisit(long visitId, @NonNull final DeleteVisitCallbackIf callback) {
        checkNotNull(callback, "callback can not be null");

        mVisitsEndpoint.deleteVisit(visitId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                onDeleteVisitResponse(response, callback);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onDeleteVisitFailure();
            }
        });
    }

    @Override
    public void getVisitDetails(long visitId, @NonNull final GetVisitDetailsCallbackIf callback) {
        checkNotNull(callback, "callback can not be null");

        mVisitsEndpoint.getVisitDetails(visitId).enqueue(new Callback<VisitDetails>() {
            @Override
            public void onResponse(Call<VisitDetails> call, Response<VisitDetails> response) {
                onGetVisitDetailsResponse(response, callback);
            }

            @Override
            public void onFailure(Call<VisitDetails> call, Throwable t) {
                callback.onGetVisitDetailsFailure();
            }
        });
    }

    private void onFindMySimpleVisitsResponse(Response<SimpleVisitsResponse> response,
                                              GetSimpleVisitsCallbackIf callback) {
        if (response.isSuccessful()) {
            callback.onGetSimpleVisitsSuccess(response.body());
        } else {
            callback.onGetSimpleVisitsFailure();
        }
    }

    private void onGetDoctorCalendarResponse(Response<Map<Date, List<CalendarEntity>>> response,
                                             GetDoctorCalendarCallbackIf callback) {
        if (response.isSuccessful()) {
            callback.onGetDoctorCalendarSuccess(response.body());
        } else {
            callback.onGetDoctorCalendarFailure();
        }
    }

    private void onCreateVisitResponse(Response<SimpleVisit> response, CreateVisitCallbackIf callback) {
        if (response.isSuccessful()) {
            callback.onCreateVisitSuccess(response.body());
        } else {
            callback.onCreateVisitFailure();
        }
    }

    private void onChangeVisitDateResponse(Response<SimpleVisit> response, ChangeVisitDateCallbackIf callback) {
        if (response.isSuccessful()) {
            callback.onChangeVisitDateSuccess(response.body());
        } else {
            callback.onChangeVisitDateFailure();
        }
    }

    private void onDeleteVisitResponse(Response<ResponseBody> response, DeleteVisitCallbackIf callback) {
        if (response.isSuccessful()) {
            callback.onDeleteVisitSuccess();
        } else {
            callback.onDeleteVisitFailure();
        }
    }

    private void onGetVisitDetailsResponse(Response<VisitDetails> response, GetVisitDetailsCallbackIf callback) {
        if (response.isSuccessful()) {
            callback.onGetVisitDetailsSuccess(response.body());
        } else {
            callback.onGetVisitDetailsFailure();
        }
    }
}
