package edu.wmi.dpri.przychodnia.android.model.data.user;

public enum UserSearchType {
    SEARCH_TYPE_PATIENT,
    SEARCH_TYPE_STAFF,
    ALL
}
