package edu.wmi.dpri.przychodnia.android.model.data.visit;

public enum VisitStatus {

    NEW,
    HAPPENED,
    CANCELLED,
    HOME_VISIT
}
