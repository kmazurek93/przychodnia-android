package edu.wmi.dpri.przychodnia.android.model.data.user;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static com.google.common.base.Preconditions.checkNotNull;

public class UserFilter {

    private final static int DEFAULT_PAGE_SIZE = 20;

    @SerializedName("name")
    private String mUserName;

    @SerializedName("mail")
    private String mUserMail;

    @SerializedName("address")
    private String mUserAddress;

    @SerializedName("telephone")
    private String mUserTelephone;

    @SerializedName("pesel")
    private String mUserPesel;

    @SerializedName("searchType")
    private UserSearchType mUserSearchType;

    @SerializedName("page")
    private int mPage;

    @SerializedName("size")
    private int mPageSize;

    public UserFilter(@NonNull UserSearchType userSearchType, int page) {
        mUserName = "";
        mUserMail = "";
        mUserAddress = "";
        mUserTelephone = "";
        mUserPesel = "";
        mUserSearchType = checkNotNull(userSearchType, "userSearchType can not be null");
        mPage = page;
        mPageSize = DEFAULT_PAGE_SIZE;
    }

    public UserSearchType getUserSearchType() {
        return mUserSearchType;
    }

    public int getPage() {
        return mPage;
    }

    public void setPage(int page) {
        mPage = page;
    }

    public int getPageSize() {
        return mPageSize;
    }

    public void setPageSize(int pageSize) {
        mPageSize = pageSize;
    }
}
