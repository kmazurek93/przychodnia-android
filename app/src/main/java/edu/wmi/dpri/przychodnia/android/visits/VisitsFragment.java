package edu.wmi.dpri.przychodnia.android.visits;

import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import edu.wmi.dpri.przychodnia.android.R;
import edu.wmi.dpri.przychodnia.android.add.visit.AddVisitActivity;
import edu.wmi.dpri.przychodnia.android.databinding.VisitsFragBinding;
import edu.wmi.dpri.przychodnia.android.edit.visit.EditVisitActivity;
import edu.wmi.dpri.przychodnia.android.util.EndlessRecyclerViewScrollListener;
import edu.wmi.dpri.przychodnia.android.visit.details.VisitDetailsActivity;

import static com.google.common.base.Preconditions.checkNotNull;

public class VisitsFragment extends Fragment implements VisitsContractIf.ViewIf {

    private VisitsContractIf.PresenterIf mVisitsPresenter;

    private VisitsAdapter mVisitsAdapter;
    private EndlessRecyclerViewScrollListener mEndlessRecyclerViewScrollListener;

    private int mLongClickedVisitPosition = -1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.visits_frag, container, false);
        VisitsFragBinding binding = VisitsFragBinding.bind(root);

        initVisitsAdapter();
        initVisitsRecyclerView(binding.visitsFragRecyclerView);
        initFloatingActionButton(binding.visitsFragFloatingActionButton);

        setHasOptionsMenu(true);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mLongClickedVisitPosition = -1;
        mVisitsPresenter.start();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_visits, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_visits_filter_new) {
            mVisitsPresenter.loadNewVisits();
        } else if (id == R.id.menu_visits_filter_happened) {
            mVisitsPresenter.loadHappenedVisits();
        } else if (id == R.id.menu_visits_filter_cancelled) {
            mVisitsPresenter.loadCancelledVisits();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_visits_context_details) {
            mVisitsPresenter.onVisitDetailSelected(mLongClickedVisitPosition);
        } else if (id == R.id.menu_visits_context_edit) {
            mVisitsPresenter.onVisitEditSelected(mLongClickedVisitPosition);
        } else if (id == R.id.menu_visits_context_cancel) {
            mVisitsPresenter.onVisitCancelSelected(mLongClickedVisitPosition);
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        //TODO
    }

    @Override
    public void refreshAllVisits() {
        mVisitsAdapter.notifyDataSetChanged();
        mEndlessRecyclerViewScrollListener.resetState();
    }

    @Override
    public void refreshVisitsInRange(int positionStart, int itemInRangeCount) {
        mVisitsAdapter.notifyItemRangeInserted(positionStart, itemInRangeCount);
    }

    @Override
    public void promptForCancelVisitConfirmation(final int visitPosition) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(getString(R.string.visits_dialog_cancelVisitConfirm_title));
        alertDialog.setMessage(getString(R.string.visits_dialog_cancelVisitConfirm_message));
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.general_dialog_close),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        dialogInterface.dismiss();
                    }
                }
        );
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                getString(R.string.visits_dialog_cancelVisitConfirm_button_confirm),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mVisitsPresenter.onVisitCancelConfirm(visitPosition);
                        dialogInterface.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void showEditVisitView(Bundle bundle) {
        Intent intent = new Intent(getActivity(), EditVisitActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void showVisitDetailsView(Bundle bundle) {
        Intent intent = new Intent(getActivity(), VisitDetailsActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void showLoadingVisitsError() {
        //TODO
    }

    @Override
    public void showVisitCancelSuccess() {
        Toast.makeText(getActivity(), R.string.visits_cancelVisit_success, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showVisitCancelError() {
        Toast.makeText(getActivity(), R.string.visits_cancelVisit_error, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(@NonNull VisitsContractIf.PresenterIf presenter) {
        mVisitsPresenter = checkNotNull(presenter, "presenter can not be null");
    }

    private void showAddVisitView() {
        Intent intent = new Intent(getActivity(), AddVisitActivity.class);
        startActivity(intent);
    }

    private void initVisitsAdapter() {
        mVisitsAdapter = new VisitsAdapter(mVisitsPresenter);
        mVisitsAdapter.setOnItemClickListener(new VisitsAdapter.OnItemClickListenerIf() {
            @Override
            public void onItemClick(View itemView, int position) {
                mVisitsPresenter.onVisitDetailSelected(position);
            }
        });
        mVisitsAdapter.setOnItemLongClickListener(new VisitsAdapter.OnItemLongClickListenerIf() {
            @Override
            public void onItemLongClick(View itemView, int position) {
                mLongClickedVisitPosition = position;
            }
        });
        mVisitsAdapter.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
                contextMenu.setHeaderTitle(R.string.visits_menu_context_title);
                switch (mVisitsPresenter.getSimpleVisit(mLongClickedVisitPosition).getVisitStatus()) {
                    case NEW:
                        contextMenu.add(Menu.NONE, R.id.menu_visits_context_details, 0, R.string.visits_menu_context_details);
                        contextMenu.add(Menu.NONE, R.id.menu_visits_context_edit, 1, R.string.visits_menu_context_edit);
                        contextMenu.add(Menu.NONE, R.id.menu_visits_context_cancel, 2, R.string.visits_menu_context_cancel);
                        break;
                    case HAPPENED:
                        contextMenu.add(Menu.NONE, R.id.menu_visits_context_details, 0, R.string.visits_menu_context_details);
                        break;
                    case CANCELLED:
                        contextMenu.add(Menu.NONE, R.id.menu_visits_context_details, 0, R.string.visits_menu_context_details);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    private void initVisitsRecyclerView(RecyclerView visitsRecyclerView) {
        visitsRecyclerView.setAdapter(mVisitsAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        visitsRecyclerView.setLayoutManager(linearLayoutManager);
//        visitsRecyclerView.addItemDecoration(
//                new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        visitsRecyclerView.setHasFixedSize(true);

        mEndlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                mVisitsPresenter.loadNextVisits(page);
            }
        };
        visitsRecyclerView.addOnScrollListener(mEndlessRecyclerViewScrollListener);
    }

    private void initFloatingActionButton(FloatingActionButton floatingActionButton) {
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddVisitView();
            }
        });
    }
}
