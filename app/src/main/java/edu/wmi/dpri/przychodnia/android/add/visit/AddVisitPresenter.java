package edu.wmi.dpri.przychodnia.android.add.visit;

import android.support.annotation.NonNull;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import edu.wmi.dpri.przychodnia.android.add.visit.util.AddVisitUtils;
import edu.wmi.dpri.przychodnia.android.model.UserType;
import edu.wmi.dpri.przychodnia.android.model.data.calendar.CalendarEntity;
import edu.wmi.dpri.przychodnia.android.model.data.calendar.CalendarFilter;
import edu.wmi.dpri.przychodnia.android.model.data.user.User;
import edu.wmi.dpri.przychodnia.android.model.data.user.UserContext;
import edu.wmi.dpri.przychodnia.android.model.data.user.UserFilter;
import edu.wmi.dpri.przychodnia.android.model.data.user.UserSearchType;
import edu.wmi.dpri.przychodnia.android.model.data.user.UsersResponse;
import edu.wmi.dpri.przychodnia.android.model.data.visit.CreateVisitRequestBody;
import edu.wmi.dpri.przychodnia.android.model.data.visit.SimpleVisit;
import edu.wmi.dpri.przychodnia.android.service.user.UsersServiceIf;
import edu.wmi.dpri.przychodnia.android.service.visits.VisitsServiceIf;
import edu.wmi.dpri.przychodnia.android.util.TimeUtils;

import static com.google.common.base.Preconditions.checkNotNull;

public class AddVisitPresenter implements AddVisitContractIf.PresenterIf {

    private AddVisitContractIf.ViewIf mAddVisitView;
    private UsersServiceIf mUsersService;
    private VisitsServiceIf mVisitsService;

    private List<User> mDoctors = new ArrayList<>();
    private List<Map<Date, List<CalendarEntity>>> mDoctorsFreeCalendarList = new ArrayList<>();

    private int mSelectedDoctorPosition = -1;
    private Calendar mSelectedDate;
    private Calendar mTmpSelectedDate;

    private boolean mFirstDoctorSelecting = true;

    @Inject
    AddVisitPresenter(@NonNull AddVisitContractIf.ViewIf addVisitView,
                      @NonNull UsersServiceIf usersService,
                      @NonNull VisitsServiceIf visitsService) {
        mAddVisitView = checkNotNull(addVisitView, "addVisitView can not be null");
        mUsersService = checkNotNull(usersService, "usersService can not be null");
        mVisitsService = checkNotNull(visitsService, "visitsService can not be null");
    }

    @Inject
    void setupListeners() {
        mAddVisitView.setPresenter(this);
    }

    @Override
    public void start() {
        mSelectedDoctorPosition = -1;
        loadDoctors();
    }

    @Override
    public List<User> getDoctors() {
        return mDoctors;
    }

    @Override
    public void onDoctorSelected(final int position) {
        if (mFirstDoctorSelecting) {
            mFirstDoctorSelecting = false;
            return;
        }
        if (position == -1 && mAddVisitView.isActive()) {
            mAddVisitView.showNoDoctorSelectedError();
            return;
        }

        mSelectedDoctorPosition = position;

        if (mDoctorsFreeCalendarList.get(position).size() == 0) {

            long doctorId = mDoctors.get(position).getEntityId();
            mVisitsService.getDoctorCalendar(new CalendarFilter(doctorId, TimeUtils.now(), TimeUtils.xMonthsLater(4)),
                    new VisitsServiceIf.GetDoctorCalendarCallbackIf() {
                        @Override
                        public void onGetDoctorCalendarSuccess(Map<Date, List<CalendarEntity>> doctorCalendar) {
                            Map<Date, List<CalendarEntity>> freeCalendar = AddVisitUtils.findFreeCalendarEntity(doctorCalendar);
                            mDoctorsFreeCalendarList.get(position).putAll(freeCalendar);

                            if (mAddVisitView.isActive()) {
                                mAddVisitView.showDatePickerDialogWithSelectableDays(
                                        AddVisitUtils.convertDateCollectionToCalendarList(freeCalendar.keySet()));
                            }
                        }

                        @Override
                        public void onGetDoctorCalendarFailure() {
                            //TODO
                        }
                    });
        } else {
            mAddVisitView.showDatePickerDialogWithSelectableDays(
                    AddVisitUtils.convertDateCollectionToCalendarList(
                            mDoctorsFreeCalendarList.get(position).keySet()));
        }
    }

    @Override
    public void onDateSelect(int year, int monthOfYear, int dayOfMonth) {
        mTmpSelectedDate = new GregorianCalendar(year, monthOfYear, dayOfMonth);

        List<Timepoint> selectableTimes = new ArrayList<>();
        Map<Date, List<CalendarEntity>> selectedDoctorFreeCalendar = mDoctorsFreeCalendarList.get(mSelectedDoctorPosition);
        for (CalendarEntity calendarEntity : selectedDoctorFreeCalendar.get(mTmpSelectedDate.getTime())) {
            selectableTimes.add(TimeUtils.parseTime24Hours(calendarEntity.getTimeStart()));
        }

        mAddVisitView.showTimePickerDialogWithSelectableTimes(selectableTimes);
    }

    @Override
    public void onTimeSelect(int hourOfDay, int minute) {
        mTmpSelectedDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mTmpSelectedDate.set(Calendar.MINUTE, minute);
        mSelectedDate = mTmpSelectedDate;

        DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.SHORT);
        formatter.setTimeZone(mSelectedDate.getTimeZone());
        mAddVisitView.showSelectedDate(formatter.format(mSelectedDate.getTime()));
    }

    @Override
    public void onConfirmButtonClick() {
        if (mSelectedDoctorPosition == -1) {
            mAddVisitView.showNoDoctorSelectedError();
            return;
        }
        if (mSelectedDate == null) {
            mAddVisitView.showNoDateSelectedError();
            return;
        }

        mUsersService.getUserContext(new UsersServiceIf.GetUserContextCallbackIf() {
            @Override
            public void onGetUserContextSuccess(UserContext userContext) {
                createVisit(userContext.getPesel());
            }

            @Override
            public void onGetUserContextFailure() {
                //TODO
            }
        });
    }

    private void loadDoctors() {
        mUsersService.getUsers(new UserFilter(UserSearchType.SEARCH_TYPE_STAFF, 0),
                new UsersServiceIf.GetUsersCallbackIf() {
                    @Override
                    public void onGetUsersSuccess(UsersResponse usersResponse) {
                        for (User doctor : findDoctors(usersResponse.getUsers())) {
                            mDoctors.add(doctor);
                            mDoctorsFreeCalendarList.add(new HashMap<Date, List<CalendarEntity>>());
                        }
                        if (mAddVisitView.isActive()) {
                            mAddVisitView.refreshAllDoctors();
                        }
                    }

                    @Override
                    public void onGetUsersFailure() {
                        if (mAddVisitView.isActive()) {
                            mAddVisitView.showLoadingDoctorsError();
                        }
                    }
                });
    }

    private void createVisit(String patientPesel) {
        CreateVisitRequestBody createVisitRequestBody = new CreateVisitRequestBody(
                mDoctors.get(mSelectedDoctorPosition).getEntityId(),
                patientPesel,
                mSelectedDate.getTimeInMillis()
        );
        mVisitsService.createVisit(createVisitRequestBody, new VisitsServiceIf.CreateVisitCallbackIf() {
            @Override
            public void onCreateVisitSuccess(SimpleVisit simpleVisit) {
                if (mAddVisitView.isActive()) {
                    mAddVisitView.showCreateVisitSuccess();
                    mAddVisitView.showPreviousView();
                }
            }

            @Override
            public void onCreateVisitFailure() {
                if (mAddVisitView.isActive()) {
                    mAddVisitView.showCreateVisitError();
                }
            }
        });
    }

    private List<User> findDoctors(List<User> users) {
        List<User> doctors = new ArrayList<>();
        for (User user : users) {
            if (user.getUserType() == UserType.DOCTOR) {
                doctors.add(user);
            }
        }
        return doctors;
    }
}
