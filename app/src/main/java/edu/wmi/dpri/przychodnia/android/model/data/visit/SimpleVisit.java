package edu.wmi.dpri.przychodnia.android.model.data.visit;

import com.google.gson.annotations.SerializedName;

public class SimpleVisit {

    @SerializedName("visitId")
    private long mVisitId;

    @SerializedName("doctorId")
    private long mDoctorId;

    @SerializedName("patientPesel")
    private String mPatientPesel;

    @SerializedName("patientName")
    private String mPatientName;

    @SerializedName("start")
    private long mStartTimestamp;

    @SerializedName("end")
    private long mEndTimestamp;

    @SerializedName("status")
    private VisitStatus mVisitStatus;

    private SimpleVisit() {
    }

    public long getVisitId() {
        return mVisitId;
    }

    public long getDoctorId() {
        return mDoctorId;
    }

    public String getPatientPesel() {
        return mPatientPesel;
    }

    public String getPatientName() {
        return mPatientName;
    }

    public long getStartTimestamp() {
        return mStartTimestamp;
    }

    public long getEndTimestamp() {
        return mEndTimestamp;
    }

    public VisitStatus getVisitStatus() {
        return mVisitStatus;
    }
}
