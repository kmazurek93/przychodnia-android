package edu.wmi.dpri.przychodnia.android.service.auth;

import android.support.annotation.Nullable;

public class AuthRefreshToken {

    @Nullable
    private String mRefreshToken;

    @Nullable
    public String getRefreshToken() {
        return mRefreshToken;
    }

    public void setNewRefreshToken(@Nullable String newRefreshToken) {
        mRefreshToken = newRefreshToken;
    }

    public void invalidate() {
        mRefreshToken = null;
    }
}
