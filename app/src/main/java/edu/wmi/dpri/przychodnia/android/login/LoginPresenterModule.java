package edu.wmi.dpri.przychodnia.android.login;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import edu.wmi.dpri.przychodnia.android.service.sign.in.LoginService;
import edu.wmi.dpri.przychodnia.android.service.sign.in.LoginServiceIf;

import static com.google.common.base.Preconditions.checkNotNull;

@Module
class LoginPresenterModule {

    private final LoginContractIf.ViewIf mLoginView;

    LoginPresenterModule(@NonNull LoginContractIf.ViewIf loginView) {
        mLoginView = checkNotNull(loginView, "loginView can not be null");
    }

    @Provides
    LoginContractIf.ViewIf provideLoginView() {
        return mLoginView;
    }

    @Provides
    LoginServiceIf provideLoginService(LoginService loginService) {
        return loginService;
    }
}
