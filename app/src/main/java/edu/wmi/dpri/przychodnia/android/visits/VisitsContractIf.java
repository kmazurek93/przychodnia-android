package edu.wmi.dpri.przychodnia.android.visits;

import android.os.Bundle;

import edu.wmi.dpri.przychodnia.android.BasePresenterIf;
import edu.wmi.dpri.przychodnia.android.BaseViewIf;
import edu.wmi.dpri.przychodnia.android.model.data.visit.SimpleVisit;

public interface VisitsContractIf {

    interface ViewIf extends BaseViewIf<PresenterIf> {

        void setLoadingIndicator(boolean active);

        void refreshAllVisits();

        void refreshVisitsInRange(int positionStart, int itemInRangeCount);

        void promptForCancelVisitConfirmation(int visitPosition);

        void showEditVisitView(Bundle bundle);

        void showVisitDetailsView(Bundle bundle);

        void showLoadingVisitsError();

        void showVisitCancelSuccess();

        void showVisitCancelError();

        boolean isActive();
    }

    interface PresenterIf extends BasePresenterIf {

        void loadNextVisits(int offset);

        void loadNewVisits();

        void loadHappenedVisits();

        void loadCancelledVisits();

        void onVisitDetailSelected(int visitPosition);

        void onVisitEditSelected(int visitPosition);

        void onVisitCancelSelected(int visitPosition);

        void onVisitCancelConfirm(int visitPosition);

        int getVisitsCount();

        SimpleVisit getSimpleVisit(int position);
    }
}
