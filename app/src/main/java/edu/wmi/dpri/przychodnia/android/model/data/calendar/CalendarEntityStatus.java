package edu.wmi.dpri.przychodnia.android.model.data.calendar;

import com.google.gson.annotations.SerializedName;

public enum CalendarEntityStatus {
    @SerializedName("free")FREE,
    @SerializedName("occupied")OCCUPIED,
    @SerializedName("unavailable")UNAVAILABLE,

    @SerializedName("new-year")NEW_YEAR,
    @SerializedName("3-kings-day")THREE_KINGS_DAY,
    @SerializedName("easter")EASTER,
    @SerializedName("easter-monday")EASTER_MONDAY,
    @SerializedName("labour-day")LABOUR_DAY,
    @SerializedName("constitution-day")CONSTITUTION_DAY,
    @SerializedName("army-day")ARMY_DAY,
    @SerializedName("all-saints-day")ALL_SAINTS_DAY,
    @SerializedName("independence-day")INDEPENDENCE_DAY,
    @SerializedName("first-christmas-day")FIRST_CHRISTMAS_DAY,
    @SerializedName("second-christmas-day")SECOND_CHRISTMAS_DAY
}
