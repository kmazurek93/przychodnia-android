package edu.wmi.dpri.przychodnia.android.edit.user.data;

import android.support.annotation.NonNull;

import static com.google.common.base.Preconditions.checkNotNull;

public class EditUserDataPresenter implements EditUserDataContractIf.PresenterIf {

    private EditUserDataContractIf.ViewIf mEditUserDataView;

    public EditUserDataPresenter(@NonNull EditUserDataContractIf.ViewIf editUserDataView) {
        mEditUserDataView = checkNotNull(editUserDataView, "editUserDataView can not be null");
    }

    @Override
    public void start() {

    }
}
