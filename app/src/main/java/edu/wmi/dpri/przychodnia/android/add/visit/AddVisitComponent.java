package edu.wmi.dpri.przychodnia.android.add.visit;

import dagger.Component;
import edu.wmi.dpri.przychodnia.android.FragmentScoped;
import edu.wmi.dpri.przychodnia.android.service.ServicesComponent;

@FragmentScoped
@Component(dependencies = ServicesComponent.class, modules = AddVisitPresenterModule.class)
interface AddVisitComponent {

    AddVisitPresenter makeAddVisitPresenter();
}
