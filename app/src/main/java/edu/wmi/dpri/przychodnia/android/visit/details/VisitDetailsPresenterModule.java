package edu.wmi.dpri.przychodnia.android.visit.details;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import dagger.Module;
import dagger.Provides;
import edu.wmi.dpri.przychodnia.android.service.visits.VisitsService;
import edu.wmi.dpri.przychodnia.android.service.visits.VisitsServiceIf;

import static com.google.common.base.Preconditions.checkNotNull;

@Module
public class VisitDetailsPresenterModule {

    private final VisitDetailsContractIf.ViewIf mVisitDetailsView;
    private final Bundle mBundle;

    VisitDetailsPresenterModule(@NonNull VisitDetailsContractIf.ViewIf visitDetailsView,
                                @Nullable Bundle bundle) {
        mVisitDetailsView = checkNotNull(visitDetailsView, "visitDetailsView can not be null");
        mBundle = bundle;
    }

    @Provides
    VisitDetailsContractIf.ViewIf provideVisitDetailsView() {
        return mVisitDetailsView;
    }

    @Provides
    @Nullable
    Bundle provideBundle() {
        return mBundle;
    }

    @Provides
    VisitsServiceIf provideVisitsService(VisitsService visitsService) {
        return visitsService;
    }
}
