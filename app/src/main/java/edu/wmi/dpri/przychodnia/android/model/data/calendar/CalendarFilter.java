package edu.wmi.dpri.przychodnia.android.model.data.calendar;

import com.google.gson.annotations.SerializedName;

public class CalendarFilter {

    @SerializedName("doctorId")
    private long mDoctorId;

    @SerializedName("startDate")
    private long mStartDate;

    @SerializedName("endDate")
    private long mEndDate;

    public CalendarFilter(long doctorId, long startDate, long endDate) {
        mDoctorId = doctorId;
        mStartDate = startDate;
        mEndDate = endDate;
    }

    public long getDoctorId() {
        return mDoctorId;
    }

    public long getStartDate() {
        return mStartDate;
    }

    public long getEndDate() {
        return mEndDate;
    }
}
