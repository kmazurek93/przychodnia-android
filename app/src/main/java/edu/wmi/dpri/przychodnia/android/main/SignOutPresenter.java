package edu.wmi.dpri.przychodnia.android.main;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import edu.wmi.dpri.przychodnia.android.service.sign.out.SignOutServiceIf;

import static com.google.common.base.Preconditions.checkNotNull;

class SignOutPresenter implements SignOutContractIf.PresenterIf, SignOutServiceIf.SignOutCallbackIf {

    private SignOutContractIf.ViewIf mSignOutView;
    private SignOutServiceIf mSignOutService;

    @Inject
    SignOutPresenter(@NonNull SignOutContractIf.ViewIf signOutView, @NonNull SignOutServiceIf signOutService) {
        mSignOutView = checkNotNull(signOutView, "signOutView can not be null");
        mSignOutService = checkNotNull(signOutService, "signOutService can not be null");
    }

    @Inject
    void setupListeners() {
        mSignOutView.setPresenter(this);
    }

    @Override
    public void signOut() {
        mSignOutService.signOut(this);
    }

    @Override
    public void onSignOutSuccess() {
        mSignOutView.showSignInView();
    }

    @Override
    public void start() {

    }
}
