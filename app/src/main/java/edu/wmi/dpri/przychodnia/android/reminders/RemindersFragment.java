package edu.wmi.dpri.przychodnia.android.reminders;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.wmi.dpri.przychodnia.android.R;

import static com.google.common.base.Preconditions.checkNotNull;

public class RemindersFragment extends Fragment implements RemindersContractIf.ViewIf {

    private RemindersContractIf.PresenterIf mRemindersPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.reminders_frag, container, false);
    }

    @Override
    public void setPresenter(@NonNull RemindersContractIf.PresenterIf presenter) {
        mRemindersPresenter = checkNotNull(presenter, "presenter can not be null");
    }
}
