package edu.wmi.dpri.przychodnia.android.endpoint;

import android.support.annotation.NonNull;

import edu.wmi.dpri.przychodnia.android.model.data.user.UserContext;
import edu.wmi.dpri.przychodnia.android.model.data.user.UserFilter;
import edu.wmi.dpri.przychodnia.android.model.data.user.UsersResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface UsersEndpointIf {

    @POST("userManagement/users")
    Call<UsersResponse> findUsers(@NonNull @Body UserFilter userFilter);

    @GET("security/userContext")
    Call<UserContext> getUserContext();
}
