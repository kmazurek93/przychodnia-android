package edu.wmi.dpri.przychodnia.android.model.data.user;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UsersResponse {

    @SerializedName("amountOfPages")
    private int mNumberOfPages;

    @SerializedName("users")
    private List<User> mUsers;

    private UsersResponse() {
    }

    public int getNumberOfPages() {
        return mNumberOfPages;
    }

    public List<User> getUsers() {
        return mUsers;
    }
}
