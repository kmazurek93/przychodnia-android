package edu.wmi.dpri.przychodnia.android.visit.details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import edu.wmi.dpri.przychodnia.android.R;
import edu.wmi.dpri.przychodnia.android.app.PrzychodniaApplication;
import edu.wmi.dpri.przychodnia.android.util.ActivityUtils;

public class VisitDetailsActivity extends AppCompatActivity {

    public static final String BUNDLE_KEY_VISIT_ID = "edu.wmi.dpri.przychodnia.android.visit.details.VisitDetailsActivity.BUNDLE_KEY_VISIT_ID";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.visit_details_act);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setSubtitle(R.string.visitDetails_title);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        VisitDetailsFragment visitDetailsFragment = new VisitDetailsFragment();
        ActivityUtils.addFragmentToActivity(getFragmentManager(), visitDetailsFragment,
                R.id.visitDetailsAct_frameLayout);

        DaggerVisitDetailsComponent.builder()
                .visitDetailsPresenterModule(new VisitDetailsPresenterModule(visitDetailsFragment, getIntent().getExtras()))
                .servicesComponent(((PrzychodniaApplication) getApplication()).getServicesComponent())
                .build()
                .makeVisitDetailsPresenter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_visit_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
