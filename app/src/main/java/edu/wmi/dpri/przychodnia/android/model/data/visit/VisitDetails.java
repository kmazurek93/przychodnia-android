package edu.wmi.dpri.przychodnia.android.model.data.visit;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VisitDetails {

    @SerializedName("visitId")
    private long mVisitId;

    @SerializedName("doctorId")
    private long mDoctorId;

    @SerializedName("patientPesel")
    private String mPatientPesel;

    @SerializedName("patientName")
    private String mPatientName;

    @SerializedName("start")
    private long mStartTimestamp;

    @SerializedName("end")
    private long mEndTimestamp;

    @SerializedName("status")
    private VisitStatus mVisitStatus;

    @SerializedName("doctorName")
    private String mDoctorName;

    @SerializedName("comment")
    private String mComment;

    @Nullable
    @SerializedName("parentVisitId")
    private Long mParentVisitId;

    @SerializedName("childVisitsIds")
    private List<Long> mChildVisitsIds;

    private VisitDetails() {
    }

    public long getVisitId() {
        return mVisitId;
    }

    public long getDoctorId() {
        return mDoctorId;
    }

    public String getPatientPesel() {
        return mPatientPesel;
    }

    public String getPatientName() {
        return mPatientName;
    }

    public long getStartTimestamp() {
        return mStartTimestamp;
    }

    public long getEndTimestamp() {
        return mEndTimestamp;
    }

    public VisitStatus getVisitStatus() {
        return mVisitStatus;
    }

    public String getDoctorName() {
        return mDoctorName;
    }

    public String getComment() {
        return mComment;
    }

    @Nullable
    public Long getParentVisitId() {
        return mParentVisitId;
    }

    public List<Long> getChildVisitsIds() {
        return mChildVisitsIds;
    }
}
