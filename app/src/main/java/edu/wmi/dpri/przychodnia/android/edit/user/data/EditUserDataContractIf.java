package edu.wmi.dpri.przychodnia.android.edit.user.data;

import edu.wmi.dpri.przychodnia.android.BasePresenterIf;
import edu.wmi.dpri.przychodnia.android.BaseViewIf;

interface EditUserDataContractIf {

    interface ViewIf extends BaseViewIf<PresenterIf> {
    }

    interface PresenterIf extends BasePresenterIf {
    }
}
