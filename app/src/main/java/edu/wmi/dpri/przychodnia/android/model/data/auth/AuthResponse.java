package edu.wmi.dpri.przychodnia.android.model.data.auth;

import com.google.gson.annotations.SerializedName;

public class AuthResponse {

    @SerializedName("token")
    private String mToken;

    @SerializedName("refreshToken")
    private String mRefreshToken;

    private AuthResponse() {
    }

    public String getToken() {
        return mToken;
    }

    public String getRefreshToken() {
        return mRefreshToken;
    }
}
