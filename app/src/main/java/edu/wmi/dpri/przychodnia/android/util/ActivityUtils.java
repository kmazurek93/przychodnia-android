package edu.wmi.dpri.przychodnia.android.util;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;

import static com.google.common.base.Preconditions.checkNotNull;

public class ActivityUtils {
    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment fragment, @IdRes int frameId) {
        checkNotNull(fragmentManager, "fragmentManager can not be null");
        checkNotNull(fragment, "fragment can not be null");

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        //TODO: add or replace? decide
//        transaction.add(frameId, fragment);
        transaction.replace(frameId, fragment);
        transaction.commit();
    }
}
