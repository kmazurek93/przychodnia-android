package edu.wmi.dpri.przychodnia.android.edit.visit;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import dagger.Module;
import dagger.Provides;
import edu.wmi.dpri.przychodnia.android.service.user.UsersService;
import edu.wmi.dpri.przychodnia.android.service.user.UsersServiceIf;
import edu.wmi.dpri.przychodnia.android.service.visits.VisitsService;
import edu.wmi.dpri.przychodnia.android.service.visits.VisitsServiceIf;

import static com.google.common.base.Preconditions.checkNotNull;

@Module
public class EditVisitPresenterModule {

    private final EditVisitContractIf.ViewIf mEditVisitView;
    private final Bundle mBundle;

    EditVisitPresenterModule(@NonNull EditVisitContractIf.ViewIf editVisitView,
                             @Nullable Bundle bundle) {
        mEditVisitView = checkNotNull(editVisitView, "editVisitView can not be null");
        mBundle = bundle;
    }

    @Provides
    EditVisitContractIf.ViewIf provideEditVisitView() {
        return mEditVisitView;
    }

    @Provides
    @Nullable
    Bundle provideBundle() {
        return mBundle;
    }

    @Provides
    UsersServiceIf provideUsersService(UsersService usersService) {
        return usersService;
    }

    @Provides
    VisitsServiceIf provideVisitsService(VisitsService visitsService) {
        return visitsService;
    }
}
