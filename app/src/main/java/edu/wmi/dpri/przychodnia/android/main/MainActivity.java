package edu.wmi.dpri.przychodnia.android.main;

import android.app.Fragment;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import edu.wmi.dpri.przychodnia.android.R;
import edu.wmi.dpri.przychodnia.android.app.PrzychodniaApplication;
import edu.wmi.dpri.przychodnia.android.databinding.MainActBinding;
import edu.wmi.dpri.przychodnia.android.login.LoginActivity;
import edu.wmi.dpri.przychodnia.android.reminders.RemindersFragment;
import edu.wmi.dpri.przychodnia.android.reminders.RemindersPresenter;
import edu.wmi.dpri.przychodnia.android.visits.DaggerVisitsComponent;
import edu.wmi.dpri.przychodnia.android.visits.VisitsContractIf;
import edu.wmi.dpri.przychodnia.android.visits.VisitsFragment;
import edu.wmi.dpri.przychodnia.android.visits.VisitsPresenterModule;

import static com.google.common.base.Preconditions.checkNotNull;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SignOutContractIf.ViewIf {

    private SignOutContractIf.PresenterIf mSignOutPresenter;

    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MainActBinding binding = DataBindingUtil.setContentView(this, R.layout.main_act);
        Toolbar toolbar = binding.mainActMainContent.mainContentToolbar;
        setSupportActionBar(toolbar);

        mDrawerLayout = binding.mainActDrawerLayout;
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
                R.string.main_navigationDrawer_open, R.string.main_navigationDrawer_close);
        mDrawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        mNavigationView = binding.mainActNavigationView;
        mNavigationView.setNavigationItemSelectedListener(this);

        selectDefaultNavigationItem();

        DaggerMainComponent.builder()
                .signOutPresenterModule(new SignOutPresenterModule(this))
                .servicesComponent(((PrzychodniaApplication) getApplication()).getServicesComponent())
                .build()
                .makeSignOutPresenter();
    }

    @Override
    public void onBackPressed() {
        if (isNavigationDrawerOpen()) {
            closeNavigationDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_main_logout) {
            mSignOutPresenter.signOut();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        ActionBar actionBar = getSupportActionBar();
        if (id == R.id.menu_mainNav_visits) {
            actionBar.setSubtitle(R.string.visits_title);
            VisitsFragment visitsFragment = new VisitsFragment();
            makeVisitsPresenter(visitsFragment);
            replaceFragment(visitsFragment);
        } else if (id == R.id.menu_mainNav_reminders) {
            actionBar.setSubtitle(R.string.reminders_title);
            RemindersFragment remindersFragment = new RemindersFragment();
            remindersFragment.setPresenter(new RemindersPresenter(remindersFragment));
            replaceFragment(remindersFragment);
        }

        closeNavigationDrawer();
        return true;
    }

    @Override
    public void showSignInView() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void setPresenter(@NonNull SignOutContractIf.PresenterIf presenter) {
        mSignOutPresenter = checkNotNull(presenter, "presenter can not be null");
    }

    private void replaceFragment(Fragment fragment) {
        getFragmentManager().beginTransaction().replace(R.id.mainContent_frameLayout, fragment).commit();
    }

    private boolean isNavigationDrawerOpen() {
        return mDrawerLayout.isDrawerOpen(GravityCompat.START);
    }

    private void closeNavigationDrawer() {
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    private void selectDefaultNavigationItem() {
        MenuItem defaultItem = mNavigationView.getMenu().getItem(0);
        onNavigationItemSelected(defaultItem);
        defaultItem.setChecked(true);
    }

    private void makeVisitsPresenter(VisitsContractIf.ViewIf visitsView) {
        DaggerVisitsComponent.builder()
                .visitsPresenterModule(new VisitsPresenterModule(visitsView))
                .servicesComponent(
                        ((PrzychodniaApplication) getApplication()).getServicesComponent())
                .build()
                .makeVisitsPresenter();
    }
}
