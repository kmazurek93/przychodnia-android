package edu.wmi.dpri.przychodnia.android.add.visit;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.util.Calendar;
import java.util.List;

import edu.wmi.dpri.przychodnia.android.BasePresenterIf;
import edu.wmi.dpri.przychodnia.android.BaseViewIf;
import edu.wmi.dpri.przychodnia.android.model.data.user.User;

public interface AddVisitContractIf {

    interface ViewIf extends BaseViewIf<PresenterIf> {

        void refreshAllDoctors();

        void showDatePickerDialogWithSelectableDays(List<Calendar> selectableDays);

        void showTimePickerDialogWithSelectableTimes(List<Timepoint> selectableTimes);

        void showSelectedDate(String selectedDate);

        void showCreateVisitSuccess();

        void showCreateVisitError();

        void showLoadingDoctorsError();

        void showNoDoctorSelectedError();

        void showNoDateSelectedError();

        boolean isActive();

        void showPreviousView();
    }

    interface PresenterIf extends BasePresenterIf {

        List<User> getDoctors();

        void onDoctorSelected(int position);

        void onDateSelect(int year, int monthOfYear, int dayOfMonth);

        void onTimeSelect(int hourOfDay, int minute);

        void onConfirmButtonClick();
    }
}
