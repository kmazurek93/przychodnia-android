package edu.wmi.dpri.przychodnia.android.add.visit;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import edu.wmi.dpri.przychodnia.android.service.user.UsersService;
import edu.wmi.dpri.przychodnia.android.service.user.UsersServiceIf;
import edu.wmi.dpri.przychodnia.android.service.visits.VisitsService;
import edu.wmi.dpri.przychodnia.android.service.visits.VisitsServiceIf;

import static com.google.common.base.Preconditions.checkNotNull;

@Module
class AddVisitPresenterModule {

    private final AddVisitContractIf.ViewIf mAddVisitView;

    AddVisitPresenterModule(@NonNull AddVisitContractIf.ViewIf addVisitView) {
        mAddVisitView = checkNotNull(addVisitView, "addVisitView can not be null");
    }

    @Provides
    AddVisitContractIf.ViewIf provideAddVisitView() {
        return mAddVisitView;
    }

    @Provides
    UsersServiceIf provideUsersService(UsersService usersService) {
        return usersService;
    }

    @Provides
    VisitsServiceIf provideVisitsService(VisitsService visitsService) {
        return visitsService;
    }
}
