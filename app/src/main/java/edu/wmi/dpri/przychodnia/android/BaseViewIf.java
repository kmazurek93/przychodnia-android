package edu.wmi.dpri.przychodnia.android;

import android.support.annotation.NonNull;

public interface BaseViewIf<T extends BasePresenterIf> {

    void setPresenter(@NonNull T presenter);
}
