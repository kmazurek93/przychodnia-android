package edu.wmi.dpri.przychodnia.android.login;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import edu.wmi.dpri.przychodnia.android.model.data.auth.LoginData;
import edu.wmi.dpri.przychodnia.android.service.sign.in.LoginServiceIf;

import static com.google.common.base.Preconditions.checkNotNull;

class LoginPresenter implements LoginContractIf.PresenterIf, LoginServiceIf.SignInCallbackIf {

    private LoginContractIf.ViewIf mLoginView;
    private LoginServiceIf mLoginService;

    @Inject
    LoginPresenter(@NonNull LoginContractIf.ViewIf loginView, @NonNull LoginServiceIf loginService) {
        mLoginView = checkNotNull(loginView, "loginView can not be null");
        mLoginService = checkNotNull(loginService, "loginService can not be null");
    }

    @Inject
    void setupListeners() {
        mLoginView.setPresenter(this);
    }

    @Override
    public void signIn(@NonNull LoginData loginData) {
        checkNotNull(loginData, "loginData can not be null");

        String alias = loginData.getUsername();
        String password = loginData.getPassword();

        mLoginView.clearAliasError();
        mLoginView.clearPasswordError();

        if (alias.isEmpty()) {
            mLoginView.showEmptyAliasError();
        } else if (!isAliasValid(alias)) {
            mLoginView.showInvalidAliasError();
        } else if (password.isEmpty()) {
            mLoginView.showEmptyPasswordError();
        } else if (!isPasswordValid(password)) {
            mLoginView.showInvalidPasswordError();
        } else {
            mLoginService.signIn(loginData, this);
        }
    }

    @Override
    public void start() {
    }

    @Override
    public void onSignInSuccess() {
        mLoginView.showMainView();
    }

    @Override
    public void onSignInFailure() {
        mLoginView.showSignInFailedError();
    }

    private boolean isAliasValid(String alias) {
        //TODO:
        return alias.length() >= 2;
    }

    private boolean isPasswordValid(String password) {
        //TODO:
        return password.length() >= 6;
    }
}
