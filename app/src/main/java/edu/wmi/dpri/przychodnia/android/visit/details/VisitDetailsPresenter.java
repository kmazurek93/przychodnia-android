package edu.wmi.dpri.przychodnia.android.visit.details;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import edu.wmi.dpri.przychodnia.android.edit.visit.EditVisitActivity;
import edu.wmi.dpri.przychodnia.android.model.data.visit.VisitDetails;
import edu.wmi.dpri.przychodnia.android.model.data.visit.VisitStatus;
import edu.wmi.dpri.przychodnia.android.service.visits.VisitsServiceIf;
import edu.wmi.dpri.przychodnia.android.util.TimeUtils;
import edu.wmi.dpri.przychodnia.android.util.VisitsUtils;

import static com.google.common.base.Preconditions.checkNotNull;

public class VisitDetailsPresenter implements VisitDetailsContractIf.PresenterIf {

    private VisitDetailsContractIf.ViewIf mVisitDetailsView;
    private VisitsServiceIf mVisitsService;

    private Bundle mBundle;

    private long mVisitId;
    private long mDoctorId;
    private long mStartTimestamp;
    private VisitStatus mVisitStatus;

    @Inject
    VisitDetailsPresenter(@NonNull VisitDetailsContractIf.ViewIf visitDetailsView,
                          @NonNull VisitsServiceIf visitsService,
                          @Nullable Bundle bundle) {
        mVisitDetailsView = checkNotNull(visitDetailsView, "visitDetailsView can not be null");
        mVisitsService = checkNotNull(visitsService, "visitsService can not be null");
        mBundle = bundle;
    }

    @Inject
    void setupListeners() {
        mVisitDetailsView.setPresenter(this);
    }

    @Override
    public void start() {
        if (mBundle != null) {
            long visitId = mBundle.getLong(VisitDetailsActivity.BUNDLE_KEY_VISIT_ID);

            mVisitsService.getVisitDetails(visitId, new VisitsServiceIf.GetVisitDetailsCallbackIf() {
                @Override
                public void onGetVisitDetailsSuccess(VisitDetails visitDetails) {
                    mVisitId = visitDetails.getVisitId();
                    mDoctorId = visitDetails.getDoctorId();
                    mStartTimestamp = visitDetails.getStartTimestamp();
                    mVisitStatus = visitDetails.getVisitStatus();

                    if (mVisitDetailsView.isActive()) {
                        mVisitDetailsView.showVisitStartDate(TimeUtils.timestampToPrettyDate(visitDetails.getStartTimestamp()));
                        mVisitDetailsView.showVisitEndDate(TimeUtils.timestampToPrettyDate(visitDetails.getEndTimestamp()));
                        mVisitDetailsView.showVisitStatus(VisitsUtils.prettyVisitStatusShort(visitDetails.getVisitStatus()));
                        mVisitDetailsView.showDoctorName(visitDetails.getDoctorName());
                        mVisitDetailsView.showVisitComment(visitDetails.getComment());
                    }
                }

                @Override
                public void onGetVisitDetailsFailure() {
                    if (mVisitDetailsView.isActive()) {
                        mVisitDetailsView.showGetVisitDetailsError();
                    }
                }
            });
        }
    }

    @Override
    public void onEditVisit() {
        if (mVisitStatus != VisitStatus.NEW) {
            mVisitDetailsView.showCannotEditVisitError();
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putLong(EditVisitActivity.BUNDLE_KEY_DOCTOR_ID, mDoctorId);
        bundle.putLong(EditVisitActivity.BUNDLE_KEY_START_TIMESTAMP, mStartTimestamp);
        bundle.putLong(EditVisitActivity.BUNDLE_KEY_VISIT_ID, mVisitId);

        mVisitDetailsView.showEditVisitView(bundle);
    }

    @Override
    public void onCancelVisit() {
        if (mVisitStatus != VisitStatus.NEW) {
            mVisitDetailsView.showCannotCancelVisitError();
            return;
        }
        mVisitDetailsView.promptForCancelVisitConfirmation();
    }

    @Override
    public void onVisitCancelConfirm() {
        mVisitsService.deleteVisit(mVisitId, new VisitsServiceIf.DeleteVisitCallbackIf() {
            @Override
            public void onDeleteVisitSuccess() {
                if (mVisitDetailsView.isActive()) {
                    mVisitDetailsView.showVisitCancelSuccess();
                    mVisitDetailsView.showPreviousView();
                }
            }

            @Override
            public void onDeleteVisitFailure() {
                if (mVisitDetailsView.isActive()) {
                    mVisitDetailsView.showVisitCancelError();
                }
            }
        });
    }
}
