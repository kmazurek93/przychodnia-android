package edu.wmi.dpri.przychodnia.android.main;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import edu.wmi.dpri.przychodnia.android.service.sign.out.SignOutService;
import edu.wmi.dpri.przychodnia.android.service.sign.out.SignOutServiceIf;

import static com.google.common.base.Preconditions.checkNotNull;

@Module
class SignOutPresenterModule {

    private final SignOutContractIf.ViewIf mSignOutView;

    SignOutPresenterModule(@NonNull SignOutContractIf.ViewIf signOutView) {
        mSignOutView = checkNotNull(signOutView, "signOutView can not be null");
    }

    @Provides
    SignOutContractIf.ViewIf provideSignOutView() {
        return mSignOutView;
    }

    @Provides
    SignOutServiceIf provideSignOutService(SignOutService signOutService) {
        return signOutService;
    }
}
