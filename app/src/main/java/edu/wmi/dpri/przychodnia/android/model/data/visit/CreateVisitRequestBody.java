package edu.wmi.dpri.przychodnia.android.model.data.visit;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static com.google.common.base.Preconditions.checkNotNull;

public class CreateVisitRequestBody {

    @SerializedName("doctorId")
    private long mDoctorId;

    @SerializedName("patientPesel")
    private String mPatientPesel;

    @SerializedName("start")
    private long mStartTimestamp;

    public CreateVisitRequestBody(long doctorId, @NonNull String patientPesel, long startTimestamp) {
        checkNotNull(patientPesel, "patientPesel can not be null");

        mDoctorId = doctorId;
        mPatientPesel = patientPesel;
        mStartTimestamp = startTimestamp;
    }

    public long getDoctorId() {
        return mDoctorId;
    }

    public String getPatientPesel() {
        return mPatientPesel;
    }

    public long getStartTimestamp() {
        return mStartTimestamp;
    }
}
