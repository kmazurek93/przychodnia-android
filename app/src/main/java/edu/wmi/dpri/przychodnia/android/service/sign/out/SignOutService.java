package edu.wmi.dpri.przychodnia.android.service.sign.out;

import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Singleton;

import edu.wmi.dpri.przychodnia.android.service.auth.AuthRefreshToken;
import edu.wmi.dpri.przychodnia.android.service.auth.AuthToken;

import static com.google.common.base.Preconditions.checkNotNull;

@Singleton
public class SignOutService implements SignOutServiceIf {

    private final AuthToken mAuthToken;
    private final AuthRefreshToken mAuthRefreshToken;

    @Inject
    SignOutService(@NonNull AuthToken authToken, @NonNull AuthRefreshToken authRefreshToken) {
        mAuthToken = checkNotNull(authToken, "authToken can not be null");
        mAuthRefreshToken = checkNotNull(authRefreshToken, "authRefreshToken can not be null");
    }


    @Override
    public void signOut(@NonNull SignOutCallbackIf callback) {
        mAuthToken.invalidate();
        mAuthRefreshToken.invalidate();

        callback.onSignOutSuccess();
    }
}
