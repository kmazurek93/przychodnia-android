package edu.wmi.dpri.przychodnia.android.model.data.user;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class UserContext {

    @SerializedName("username")
    private String mUsername;

    @SerializedName("firstName")
    private String mFirstName;

    @SerializedName("lastName")
    private String mLastName;

    @Nullable
    @SerializedName("userId")
    private Long mUserId;

    @Nullable
    @SerializedName("patientId")
    private Long mPatientId;

    @Nullable
    @SerializedName("doctorId")
    private Long mDoctorId;

    @Nullable
    @SerializedName("employeeId")
    private Long mEmployeeId;

    @SerializedName("pesel")
    private String mPesel;

//    @SerializedName("rolesAssigned")
//    private List<UserRole> mUserRole;

    private UserContext() {
    }

    public String getUsername() {
        return mUsername;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    @Nullable
    public Long getUserId() {
        return mUserId;
    }

    @Nullable
    public Long getPatientId() {
        return mPatientId;
    }

    @Nullable
    public Long getDoctorId() {
        return mDoctorId;
    }

    @Nullable
    public Long getEmployeeId() {
        return mEmployeeId;
    }

    public String getPesel() {
        return mPesel;
    }

//    public List<UserRole> getUserRole() {
//        return mUserRole;
//    }
}
