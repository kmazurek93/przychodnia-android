package edu.wmi.dpri.przychodnia.android.visits;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import edu.wmi.dpri.przychodnia.android.service.user.UsersService;
import edu.wmi.dpri.przychodnia.android.service.user.UsersServiceIf;
import edu.wmi.dpri.przychodnia.android.service.visits.VisitsService;
import edu.wmi.dpri.przychodnia.android.service.visits.VisitsServiceIf;

import static com.google.common.base.Preconditions.checkNotNull;

@Module
public class VisitsPresenterModule {

    private final VisitsContractIf.ViewIf mVisitsView;

    public VisitsPresenterModule(@NonNull VisitsContractIf.ViewIf visitsView) {
        mVisitsView = checkNotNull(visitsView, "visitsView can not be null");
    }

    @Provides
    VisitsContractIf.ViewIf provideVisitsView() {
        return mVisitsView;
    }

    @Provides
    VisitsServiceIf provideVisitsService(VisitsService visitsService) {
        return visitsService;
    }

    @Provides
    UsersServiceIf provideUsersService(UsersService usersService) {
        return usersService;
    }
}
