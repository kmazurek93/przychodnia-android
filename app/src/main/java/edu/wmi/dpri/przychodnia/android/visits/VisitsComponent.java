package edu.wmi.dpri.przychodnia.android.visits;

import dagger.Component;
import edu.wmi.dpri.przychodnia.android.FragmentScoped;
import edu.wmi.dpri.przychodnia.android.service.ServicesComponent;

@FragmentScoped
@Component(dependencies = ServicesComponent.class, modules = VisitsPresenterModule.class)
public interface VisitsComponent {

    VisitsPresenter makeVisitsPresenter();
}
