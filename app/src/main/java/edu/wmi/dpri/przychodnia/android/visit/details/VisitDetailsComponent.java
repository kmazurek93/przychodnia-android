package edu.wmi.dpri.przychodnia.android.visit.details;

import dagger.Component;
import edu.wmi.dpri.przychodnia.android.FragmentScoped;
import edu.wmi.dpri.przychodnia.android.service.ServicesComponent;

@FragmentScoped
@Component(dependencies = ServicesComponent.class, modules = VisitDetailsPresenterModule.class)
interface VisitDetailsComponent {

    VisitDetailsPresenter makeVisitDetailsPresenter();
}
