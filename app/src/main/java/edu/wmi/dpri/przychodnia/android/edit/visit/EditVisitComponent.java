package edu.wmi.dpri.przychodnia.android.edit.visit;

import dagger.Component;
import edu.wmi.dpri.przychodnia.android.FragmentScoped;
import edu.wmi.dpri.przychodnia.android.service.ServicesComponent;

@FragmentScoped
@Component(dependencies = ServicesComponent.class, modules = EditVisitPresenterModule.class)
interface EditVisitComponent {

    EditVisitPresenter makeEditVisitPresenter();
}
