package edu.wmi.dpri.przychodnia.android.model.data.visit;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SimpleVisitsResponse {

    @SerializedName("numberOfPages")
    private int mNumberOfPages;

    @SerializedName("list")
    private List<SimpleVisit> mSimpleVisits;

    private SimpleVisitsResponse() {
    }

    public int getNumberOfPages() {
        return mNumberOfPages;
    }

    public List<SimpleVisit> getSimpleVisits() {
        return mSimpleVisits;
    }
}
