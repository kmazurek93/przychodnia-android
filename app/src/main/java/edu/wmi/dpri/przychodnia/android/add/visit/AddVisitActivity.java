package edu.wmi.dpri.przychodnia.android.add.visit;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import edu.wmi.dpri.przychodnia.android.R;
import edu.wmi.dpri.przychodnia.android.app.PrzychodniaApplication;
import edu.wmi.dpri.przychodnia.android.util.ActivityUtils;

public class AddVisitActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_visit_act);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setSubtitle(R.string.addVisit_title);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        AddVisitFragment addVisitFragment = new AddVisitFragment();
        ActivityUtils.addFragmentToActivity(getFragmentManager(), addVisitFragment,
                R.id.addVisitAct_frameLayout);

        DaggerAddVisitComponent.builder()
                .addVisitPresenterModule(new AddVisitPresenterModule(addVisitFragment))
                .servicesComponent(((PrzychodniaApplication) getApplication()).getServicesComponent())
                .build()
                .makeAddVisitPresenter();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
