package edu.wmi.dpri.przychodnia.android.edit.visit;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import edu.wmi.dpri.przychodnia.android.R;
import edu.wmi.dpri.przychodnia.android.app.PrzychodniaApplication;
import edu.wmi.dpri.przychodnia.android.util.ActivityUtils;

public class EditVisitActivity extends AppCompatActivity {

    public static final String BUNDLE_KEY_DOCTOR_ID = "edu.wmi.dpri.przychodnia.android.edit.visit.EditVisitActivity.BUNDLE_KEY_DOCTOR_ID";
    public static final String BUNDLE_KEY_START_TIMESTAMP = "edu.wmi.dpri.przychodnia.android.edit.visit.EditVisitActivity.BUNDLE_KEY_START_TIMESTAMP";
    public static final String BUNDLE_KEY_VISIT_ID = "edu.wmi.dpri.przychodnia.android.edit.visit.EditVisitActivity.BUNDLE_KEY_VISIT_ID";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_visit_act);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setSubtitle(R.string.editVisit_title);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        EditVisitFragment editVisitFragment = new EditVisitFragment();
        ActivityUtils.addFragmentToActivity(getFragmentManager(), editVisitFragment,
                R.id.editVisitAct_frameLayout);

        DaggerEditVisitComponent.builder()
                .editVisitPresenterModule(
                        new EditVisitPresenterModule(editVisitFragment, getIntent().getExtras()))
                .servicesComponent(((PrzychodniaApplication) getApplication()).getServicesComponent())
                .build()
                .makeEditVisitPresenter();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
