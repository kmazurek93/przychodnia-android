package edu.wmi.dpri.przychodnia.android.service.sign.out;

import android.support.annotation.NonNull;

public interface SignOutServiceIf {

    interface SignOutCallbackIf {

        void onSignOutSuccess();
    }

    void signOut(@NonNull SignOutCallbackIf callback);
}
