package edu.wmi.dpri.przychodnia.android.visit.details;

import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import edu.wmi.dpri.przychodnia.android.R;
import edu.wmi.dpri.przychodnia.android.databinding.VisitDetailsFragBinding;
import edu.wmi.dpri.przychodnia.android.edit.visit.EditVisitActivity;

import static com.google.common.base.Preconditions.checkNotNull;

public class VisitDetailsFragment extends Fragment implements VisitDetailsContractIf.ViewIf {

    private VisitDetailsContractIf.PresenterIf mVisitDetailsPresenter;

    private TextView mStartTextView;
    private TextView mEndTextView;
    private TextView mStatusTextView;
    private TextView mDoctorNameTextView;
    private TextView mCommentTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.visit_details_frag, container, false);
        VisitDetailsFragBinding binding = VisitDetailsFragBinding.bind(root);

        mStartTextView = binding.visitDetailsFragStartTextView;
        mEndTextView = binding.visitDetailsFragEndTextView;
        mStatusTextView = binding.visitDetailsFragStatusTextView;
        mDoctorNameTextView = binding.visitDetailsFragDoctorNameTextView;
        mCommentTextView = binding.visitDetailsFragCommentTextView;

        setHasOptionsMenu(true);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mVisitDetailsPresenter.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_visitDetails_edit) {
            mVisitDetailsPresenter.onEditVisit();
        } else if (itemId == R.id.menu_visitDetails_cancel) {
            mVisitDetailsPresenter.onCancelVisit();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showVisitStartDate(String visitStartDate) {
        mStartTextView.setText(visitStartDate);
    }

    @Override
    public void showVisitEndDate(String visitEndDate) {
        mEndTextView.setText(visitEndDate);
    }

    @Override
    public void showVisitStatus(String visitStatus) {
        mStatusTextView.setText(visitStatus);
    }

    @Override
    public void showDoctorName(String doctorName) {
        mDoctorNameTextView.setText(doctorName);
    }

    @Override
    public void showVisitComment(String visitComment) {
        mCommentTextView.setText(visitComment);
    }

    @Override
    public void showEditVisitView(Bundle bundle) {
        Intent intent = new Intent(getActivity(), EditVisitActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void promptForCancelVisitConfirmation() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(getString(R.string.visits_dialog_cancelVisitConfirm_title));
        alertDialog.setMessage(getString(R.string.visits_dialog_cancelVisitConfirm_message));
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.general_dialog_close),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        dialogInterface.dismiss();
                    }
                }
        );
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                getString(R.string.visits_dialog_cancelVisitConfirm_button_confirm),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mVisitDetailsPresenter.onVisitCancelConfirm();
                        dialogInterface.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void showVisitCancelSuccess() {
        Toast.makeText(getActivity(), R.string.visits_cancelVisit_success, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showVisitCancelError() {
        Toast.makeText(getActivity(), R.string.visits_cancelVisit_error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showPreviousView() {
        getActivity().onBackPressed();
    }

    @Override
    public void showGetVisitDetailsError() {

    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void showCannotEditVisitError() {
        Toast.makeText(getActivity(), R.string.visitDetails_cannotEditVisit_error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showCannotCancelVisitError() {
        Toast.makeText(getActivity(), R.string.visitDetails_cannotCancelVisit_error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setPresenter(@NonNull VisitDetailsContractIf.PresenterIf presenter) {
        mVisitDetailsPresenter = checkNotNull(presenter, "presenter can not be null");
    }
}
