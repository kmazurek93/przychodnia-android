package edu.wmi.dpri.przychodnia.android.login;

import android.support.annotation.NonNull;

import edu.wmi.dpri.przychodnia.android.BasePresenterIf;
import edu.wmi.dpri.przychodnia.android.BaseViewIf;
import edu.wmi.dpri.przychodnia.android.model.data.auth.LoginData;

interface LoginContractIf {

    interface ViewIf extends BaseViewIf<PresenterIf> {

        void showEmptyAliasError();

        void showEmptyPasswordError();

        void showInvalidAliasError();

        void showInvalidPasswordError();

        void clearAliasError();

        void clearPasswordError();

        void showSignInFailedError();

        void showMainView();
    }

    interface PresenterIf extends BasePresenterIf {

        void signIn(@NonNull LoginData loginData);
    }
}
