package edu.wmi.dpri.przychodnia.android.service.auth;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AuthTokenModule {

    @Provides
    @Singleton
    AuthToken provideAuthToken() {
        return new AuthToken();
    }

    @Provides
    @Singleton
    AuthRefreshToken provideAuthRefreshToken() {
        return new AuthRefreshToken();
    }
}
