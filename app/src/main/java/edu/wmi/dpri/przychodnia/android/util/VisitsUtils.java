package edu.wmi.dpri.przychodnia.android.util;

import edu.wmi.dpri.przychodnia.android.model.data.visit.VisitStatus;

public class VisitsUtils {

    private static final String VISIT_STATUS_LABEL = "Status wizyty: ";
    private static final String VISIT_STATUS_LABEL_SHORT = "Wizyta ";

    public static String prettyVisitStatus(VisitStatus visitStatus) {
        return prettyVisitStatusCore(visitStatus, VISIT_STATUS_LABEL);
    }

    public static String prettyVisitStatusShort(VisitStatus visitStatus) {
        return prettyVisitStatusCore(visitStatus, VISIT_STATUS_LABEL_SHORT);
    }

    private static String prettyVisitStatusCore(VisitStatus visitStatus, String label) {
        switch (visitStatus) {
            case NEW:
                return label + "aktywna";
            case HAPPENED:
                return label + "zakończona";
            case CANCELLED:
                return label + "odwołana";
            case HOME_VISIT:
                return label + "domowa";
        }
        throw new IllegalArgumentException("No such visit status");
    }
}
