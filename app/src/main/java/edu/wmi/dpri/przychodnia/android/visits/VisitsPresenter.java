package edu.wmi.dpri.przychodnia.android.visits;

import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import edu.wmi.dpri.przychodnia.android.edit.visit.EditVisitActivity;
import edu.wmi.dpri.przychodnia.android.model.data.visit.SimpleVisit;
import edu.wmi.dpri.przychodnia.android.model.data.visit.SimpleVisitsResponse;
import edu.wmi.dpri.przychodnia.android.model.data.visit.VisitFilter;
import edu.wmi.dpri.przychodnia.android.model.data.visit.VisitStatus;
import edu.wmi.dpri.przychodnia.android.service.visits.VisitsServiceIf;
import edu.wmi.dpri.przychodnia.android.util.TimeUtils;
import edu.wmi.dpri.przychodnia.android.visit.details.VisitDetailsActivity;

import static com.google.common.base.Preconditions.checkNotNull;

class VisitsPresenter implements VisitsContractIf.PresenterIf {

    private final static VisitStatus DEFAULT_VISIT_STATUS = VisitStatus.NEW;
    private final static int INIT_PAGE = 0;

    private VisitsContractIf.ViewIf mVisitsView;
    private VisitsServiceIf mVisitsService;

    private final List<SimpleVisit> mSimpleVisits = new ArrayList<>();

    private VisitFilter mCurrentVisitFilter;

    private int mNumberOfPages = -1;

    @Inject
    VisitsPresenter(@NonNull VisitsContractIf.ViewIf visitsView,
                    @NonNull VisitsServiceIf visitsService) {
        mVisitsView = checkNotNull(visitsView, "visitsView can not be null");
        mVisitsService = checkNotNull(visitsService, "visitsService can not be null");
    }

    @Inject
    void setupListeners() {
        mVisitsView.setPresenter(this);
    }

    @Override
    public void start() {
        mNumberOfPages = -1;
        clearAndLoadVisits(DEFAULT_VISIT_STATUS, TimeUtils.now());
    }

    private void loadVisits() {
        mVisitsView.setLoadingIndicator(true);

        mVisitsService.getSimpleVisits(mCurrentVisitFilter,
                new VisitsServiceIf.GetSimpleVisitsCallbackIf() {
                    @Override
                    public void onGetSimpleVisitsSuccess(SimpleVisitsResponse simpleVisitsResponse) {
                        mNumberOfPages = simpleVisitsResponse.getNumberOfPages();
                        List<SimpleVisit> simpleVisits = simpleVisitsResponse.getSimpleVisits();
                        int positionStart = mSimpleVisits.size();
                        mSimpleVisits.addAll(simpleVisits);

                        if (mVisitsView.isActive()) {
                            mVisitsView.refreshVisitsInRange(positionStart, simpleVisits.size());
                            mVisitsView.setLoadingIndicator(false);
                        }
                    }

                    @Override
                    public void onGetSimpleVisitsFailure() {
                        if (mVisitsView.isActive()) {
                            mVisitsView.showLoadingVisitsError();
                        }
                    }
                });
    }

    @Override
    public void loadNextVisits(int offset) {
        if (offset < mNumberOfPages) {
            mCurrentVisitFilter.setPage(offset);
            loadVisits();
        }
    }

    @Override
    public void loadNewVisits() {
        clearAndLoadVisits(VisitStatus.NEW, TimeUtils.now());
    }

    @Override
    public void loadHappenedVisits() {
        clearAndLoadVisits(VisitStatus.HAPPENED, TimeUtils.xMonthsEarlier(6));
    }

    @Override
    public void loadCancelledVisits() {
        clearAndLoadVisits(VisitStatus.CANCELLED, TimeUtils.xMonthsEarlier(6));
    }

    @Override
    public void onVisitDetailSelected(int visitPosition) {
        SimpleVisit simpleVisit = mSimpleVisits.get(visitPosition);
        Bundle bundle = new Bundle();
        bundle.putLong(VisitDetailsActivity.BUNDLE_KEY_VISIT_ID, simpleVisit.getVisitId());

        mVisitsView.showVisitDetailsView(bundle);
    }

    @Override
    public void onVisitEditSelected(int visitPosition) {
        SimpleVisit simpleVisit = mSimpleVisits.get(visitPosition);
        Bundle bundle = new Bundle();
        bundle.putLong(EditVisitActivity.BUNDLE_KEY_DOCTOR_ID, simpleVisit.getDoctorId());
        bundle.putLong(EditVisitActivity.BUNDLE_KEY_START_TIMESTAMP, simpleVisit.getStartTimestamp());
        bundle.putLong(EditVisitActivity.BUNDLE_KEY_VISIT_ID, simpleVisit.getVisitId());

        mVisitsView.showEditVisitView(bundle);
    }

    @Override
    public void onVisitCancelSelected(int visitPosition) {
        mVisitsView.promptForCancelVisitConfirmation(visitPosition);
    }

    @Override
    public void onVisitCancelConfirm(int visitPosition) {
        long visitId = mSimpleVisits.get(visitPosition).getVisitId();
        mVisitsService.deleteVisit(visitId, new VisitsServiceIf.DeleteVisitCallbackIf() {
            @Override
            public void onDeleteVisitSuccess() {
                if (mVisitsView.isActive()) {
                    clearAndLoadVisits(VisitStatus.NEW, TimeUtils.now());
                    mVisitsView.showVisitCancelSuccess();
                }
            }

            @Override
            public void onDeleteVisitFailure() {
                if (mVisitsView.isActive()) {
                    mVisitsView.showVisitCancelError();
                }
            }
        });
    }

    @Override
    public int getVisitsCount() {
        return mSimpleVisits.size();
    }

    @Override
    public SimpleVisit getSimpleVisit(int position) {
        return mSimpleVisits.get(position);
    }

    private void clearAndLoadVisits(VisitStatus visitStatus, long fromTimestamp) {
        mSimpleVisits.clear();
        mVisitsView.refreshAllVisits();
        mCurrentVisitFilter = new VisitFilter(visitStatus, fromTimestamp, INIT_PAGE);
        loadVisits();
    }
}
