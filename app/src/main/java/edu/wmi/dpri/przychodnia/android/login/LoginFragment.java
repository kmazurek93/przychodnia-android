package edu.wmi.dpri.przychodnia.android.login;

import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import edu.wmi.dpri.przychodnia.android.R;
import edu.wmi.dpri.przychodnia.android.databinding.LoginFragBinding;
import edu.wmi.dpri.przychodnia.android.main.MainActivity;
import edu.wmi.dpri.przychodnia.android.model.data.auth.LoginData;

import static com.google.common.base.Preconditions.checkNotNull;

public class LoginFragment extends Fragment implements LoginContractIf.ViewIf {

    private LoginContractIf.PresenterIf mLoginPresenter;

    private TextInputLayout mAliasInputLayout;
    private TextInputEditText mAliasEditText;
    private TextInputLayout mPasswordInputLayout;
    private TextInputEditText mPasswordEditText;
    private Button mSignInButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.login_frag, container, false);
        LoginFragBinding binding = LoginFragBinding.bind(root);

        mAliasInputLayout = binding.loginFragAliasInputLayout;
        mAliasEditText = binding.loginFragAliasEditText;
        mPasswordInputLayout = binding.loginFragPasswordInputLayout;
        mPasswordEditText = binding.loginFragPasswordEditText;
        mSignInButton = binding.loginFragSignInButton;

        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mLoginPresenter == null) {
                    throw new IllegalStateException("LoginContractIf.PresenterIf is null");
                }
                LoginData loginData = new LoginData(mAliasEditText.getText().toString(),
                        mPasswordEditText.getText().toString());
                mLoginPresenter.signIn(loginData);
            }
        });

        return root;
    }

    @Override
    public void showEmptyAliasError() {
        mAliasInputLayout.setError(getString(R.string.login_error_requiredField));
        mAliasInputLayout.requestFocus();
    }

    @Override
    public void showEmptyPasswordError() {
        mPasswordInputLayout.setError(getString(R.string.login_error_requiredField));
        mPasswordInputLayout.requestFocus();
    }

    @Override
    public void showInvalidAliasError() {
        mAliasInputLayout.setError(getString(R.string.login_error_invalidAlias));
        mAliasInputLayout.requestFocus();
    }

    @Override
    public void showInvalidPasswordError() {
        mPasswordInputLayout.setError(getString(R.string.login_error_invalidPassword));
        mPasswordInputLayout.requestFocus();
    }

    @Override
    public void clearAliasError() {
        mAliasInputLayout.setError(null);
    }

    @Override
    public void clearPasswordError() {
        mPasswordInputLayout.setError(null);
    }

    @Override
    public void showSignInFailedError() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(getString(R.string.general_dialog_error_title));
        alertDialog.setMessage(getString(R.string.login_error_signInFailed));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,
                getString(R.string.general_dialog_close),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }
        );
        alertDialog.show();
    }

    @Override
    public void showMainView() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void setPresenter(@NonNull LoginContractIf.PresenterIf presenter) {
        mLoginPresenter = checkNotNull(presenter, "presenter can not be null");
    }
}
