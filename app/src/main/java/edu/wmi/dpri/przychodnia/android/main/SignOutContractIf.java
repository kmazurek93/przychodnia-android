package edu.wmi.dpri.przychodnia.android.main;

import edu.wmi.dpri.przychodnia.android.BasePresenterIf;
import edu.wmi.dpri.przychodnia.android.BaseViewIf;

interface SignOutContractIf {

    interface ViewIf extends BaseViewIf<PresenterIf> {

        void showSignInView();
    }

    interface PresenterIf extends BasePresenterIf {

        void signOut();
    }
}
