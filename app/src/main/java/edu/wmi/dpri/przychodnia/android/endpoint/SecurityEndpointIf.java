package edu.wmi.dpri.przychodnia.android.endpoint;

import android.support.annotation.NonNull;

import edu.wmi.dpri.przychodnia.android.model.data.auth.AuthResponse;
import edu.wmi.dpri.przychodnia.android.model.data.auth.LoginData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SecurityEndpointIf {

    @POST("security/login")
    Call<AuthResponse> signIn(@NonNull @Body LoginData loginData);
}
