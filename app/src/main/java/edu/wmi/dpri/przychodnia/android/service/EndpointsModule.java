package edu.wmi.dpri.przychodnia.android.service;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import edu.wmi.dpri.przychodnia.android.endpoint.SecurityEndpointIf;
import edu.wmi.dpri.przychodnia.android.endpoint.UsersEndpointIf;
import edu.wmi.dpri.przychodnia.android.endpoint.VisitsEndpointIf;
import retrofit2.Retrofit;

@Module
class EndpointsModule {

    @Provides
    @Singleton
    SecurityEndpointIf provideSecurityEndpoint(Retrofit retrofit) {
        return retrofit.create(SecurityEndpointIf.class);
    }

    @Provides
    @Singleton
    VisitsEndpointIf provideVisitsEndpoint(Retrofit retrofit) {
        return retrofit.create(VisitsEndpointIf.class);
    }

    @Provides
    @Singleton
    UsersEndpointIf provideUsersEndpoint(Retrofit retrofit) {
        return retrofit.create(UsersEndpointIf.class);
    }
}
