package edu.wmi.dpri.przychodnia.android.service.user;

import android.support.annotation.NonNull;

import edu.wmi.dpri.przychodnia.android.model.data.user.UserContext;
import edu.wmi.dpri.przychodnia.android.model.data.user.UserFilter;
import edu.wmi.dpri.przychodnia.android.model.data.user.UsersResponse;

public interface UsersServiceIf {

    interface GetUsersCallbackIf {

        void onGetUsersSuccess(UsersResponse usersResponse);

        void onGetUsersFailure();
    }

    void getUsers(@NonNull UserFilter userFilter, @NonNull GetUsersCallbackIf callback);


    interface GetUserContextCallbackIf {

        void onGetUserContextSuccess(UserContext userContext);

        void onGetUserContextFailure();
    }

    void getUserContext(@NonNull GetUserContextCallbackIf callback);
}
