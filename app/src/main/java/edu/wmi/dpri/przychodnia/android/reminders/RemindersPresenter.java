package edu.wmi.dpri.przychodnia.android.reminders;

import android.support.annotation.NonNull;

import static com.google.common.base.Preconditions.checkNotNull;

public class RemindersPresenter implements RemindersContractIf.PresenterIf {

    private RemindersContractIf.ViewIf mRemindersView;

    public RemindersPresenter(@NonNull RemindersContractIf.ViewIf remindersView) {
        mRemindersView = checkNotNull(remindersView, "remindersView can not be null");
    }

    @Override
    public void start() {

    }
}
