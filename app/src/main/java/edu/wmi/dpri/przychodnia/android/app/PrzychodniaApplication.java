package edu.wmi.dpri.przychodnia.android.app;

import android.app.Application;

import edu.wmi.dpri.przychodnia.android.endpoint.PrzychodniaBaseUrlProvider;
import edu.wmi.dpri.przychodnia.android.service.DaggerServicesComponent;
import edu.wmi.dpri.przychodnia.android.service.NetModule;
import edu.wmi.dpri.przychodnia.android.service.ServicesComponent;

public class PrzychodniaApplication extends Application {

    private ServicesComponent mServicesComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mServicesComponent = DaggerServicesComponent.builder()
                .netModule(new NetModule(PrzychodniaBaseUrlProvider.BASE_URL))
                .applicationModule(new ApplicationModule(getApplicationContext()))
                .build();
    }

    public ServicesComponent getServicesComponent() {
        return mServicesComponent;
    }
}
