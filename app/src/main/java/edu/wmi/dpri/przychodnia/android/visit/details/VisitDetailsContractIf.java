package edu.wmi.dpri.przychodnia.android.visit.details;

import android.os.Bundle;

import edu.wmi.dpri.przychodnia.android.BasePresenterIf;
import edu.wmi.dpri.przychodnia.android.BaseViewIf;

public interface VisitDetailsContractIf {

    interface ViewIf extends BaseViewIf<PresenterIf> {

        void showVisitStartDate(String visitStartDate);

        void showVisitEndDate(String visitEndDate);

        void showVisitStatus(String visitStatus);

        void showDoctorName(String doctorName);

        void showVisitComment(String visitComment);

        void showEditVisitView(Bundle bundle);

        void promptForCancelVisitConfirmation();

        void showVisitCancelSuccess();

        void showVisitCancelError();

        void showPreviousView();

        void showGetVisitDetailsError();

        boolean isActive();

        void showCannotEditVisitError();

        void showCannotCancelVisitError();
    }

    interface PresenterIf extends BasePresenterIf {

        void onEditVisit();

        void onCancelVisit();

        void onVisitCancelConfirm();
    }
}
