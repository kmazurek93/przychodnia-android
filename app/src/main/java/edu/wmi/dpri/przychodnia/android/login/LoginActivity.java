package edu.wmi.dpri.przychodnia.android.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import edu.wmi.dpri.przychodnia.android.R;
import edu.wmi.dpri.przychodnia.android.app.PrzychodniaApplication;
import edu.wmi.dpri.przychodnia.android.util.ActivityUtils;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_act);

        LoginFragment loginFragment = new LoginFragment();
        ActivityUtils.addFragmentToActivity(getFragmentManager(), loginFragment,
                R.id.loginAct_frameLayout);

        DaggerLoginComponent.builder()
                .loginPresenterModule(new LoginPresenterModule(loginFragment))
                .servicesComponent(((PrzychodniaApplication) getApplication()).getServicesComponent())
                .build()
                .makeLoginPresenter();

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
    }
}
