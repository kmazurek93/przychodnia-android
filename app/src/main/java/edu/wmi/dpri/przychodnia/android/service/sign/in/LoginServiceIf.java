package edu.wmi.dpri.przychodnia.android.service.sign.in;

import android.support.annotation.NonNull;

import edu.wmi.dpri.przychodnia.android.model.data.auth.LoginData;

public interface LoginServiceIf {

    interface SignInCallbackIf {

        void onSignInSuccess();

        void onSignInFailure();
    }

    void signIn(@NonNull LoginData loginData, @NonNull SignInCallbackIf callback);
}
