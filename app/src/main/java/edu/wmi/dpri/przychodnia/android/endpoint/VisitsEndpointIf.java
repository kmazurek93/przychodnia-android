package edu.wmi.dpri.przychodnia.android.endpoint;

import android.support.annotation.NonNull;

import java.util.Date;
import java.util.List;
import java.util.Map;

import edu.wmi.dpri.przychodnia.android.model.data.calendar.CalendarEntity;
import edu.wmi.dpri.przychodnia.android.model.data.calendar.CalendarFilter;
import edu.wmi.dpri.przychodnia.android.model.data.visit.ChangeDateRequestBody;
import edu.wmi.dpri.przychodnia.android.model.data.visit.CreateVisitRequestBody;
import edu.wmi.dpri.przychodnia.android.model.data.visit.SimpleVisit;
import edu.wmi.dpri.przychodnia.android.model.data.visit.SimpleVisitsResponse;
import edu.wmi.dpri.przychodnia.android.model.data.visit.VisitDetails;
import edu.wmi.dpri.przychodnia.android.model.data.visit.VisitFilter;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface VisitsEndpointIf {

    @POST("visits/my")
    Call<SimpleVisitsResponse> findMySimpleVisits(@NonNull @Body VisitFilter visitFilter);

    @POST("visits/calendar")
    Call<Map<Date, List<CalendarEntity>>> getDoctorCalendar(@NonNull @Body CalendarFilter calendarFilter);

    @POST("visits")
    Call<SimpleVisit> createVisit(@NonNull @Body CreateVisitRequestBody createVisitRequestBody);

    @PUT("visits/changeDate")
    Call<SimpleVisit> changeVisitDate(@NonNull @Body ChangeDateRequestBody changeDateRequestBody);

    @DELETE("visits/{id}")
    Call<ResponseBody> deleteVisit(@Path("id") long visitId);

    @GET("visits/{id}")
    Call<VisitDetails> getVisitDetails(@Path("id") long visitId);
}
