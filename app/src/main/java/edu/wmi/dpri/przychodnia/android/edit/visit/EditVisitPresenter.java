package edu.wmi.dpri.przychodnia.android.edit.visit;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import edu.wmi.dpri.przychodnia.android.add.visit.util.AddVisitUtils;
import edu.wmi.dpri.przychodnia.android.model.UserType;
import edu.wmi.dpri.przychodnia.android.model.data.calendar.CalendarEntity;
import edu.wmi.dpri.przychodnia.android.model.data.calendar.CalendarFilter;
import edu.wmi.dpri.przychodnia.android.model.data.user.User;
import edu.wmi.dpri.przychodnia.android.model.data.user.UserFilter;
import edu.wmi.dpri.przychodnia.android.model.data.user.UserSearchType;
import edu.wmi.dpri.przychodnia.android.model.data.user.UsersResponse;
import edu.wmi.dpri.przychodnia.android.model.data.visit.ChangeDateRequestBody;
import edu.wmi.dpri.przychodnia.android.model.data.visit.SimpleVisit;
import edu.wmi.dpri.przychodnia.android.service.user.UsersServiceIf;
import edu.wmi.dpri.przychodnia.android.service.visits.VisitsServiceIf;
import edu.wmi.dpri.przychodnia.android.util.TimeUtils;

import static com.google.common.base.Preconditions.checkNotNull;

public class EditVisitPresenter implements EditVisitContractIf.PresenterIf {

    private EditVisitContractIf.ViewIf mEditVisitView;
    private UsersServiceIf mUsersService;
    private VisitsServiceIf mVisitsService;

    private List<User> mDoctors = new ArrayList<>();
    private Map<Date, List<CalendarEntity>> mDoctorsFreeCalendar;

    private Calendar mSelectedDate;
    private Calendar mTmpSelectedDate;

    private Bundle mBundle;

    @Inject
    EditVisitPresenter(@NonNull EditVisitContractIf.ViewIf editVisitView,
                       @NonNull UsersServiceIf usersService,
                       @NonNull VisitsServiceIf visitsService,
                       @Nullable Bundle bundle) {
        mEditVisitView = checkNotNull(editVisitView, "editVisitView can not be null");
        mUsersService = checkNotNull(usersService, "usersService can not be null");
        mVisitsService = checkNotNull(visitsService, "visitsService can not be null");
        mBundle = bundle;
    }

    @Inject
    void setupListeners() {
        mEditVisitView.setPresenter(this);
    }

    @Override
    public void start() {
        if (mBundle != null) {
            long doctorId = mBundle.getLong(EditVisitActivity.BUNDLE_KEY_DOCTOR_ID);
            loadDoctor(doctorId);

            long startTimestamp = mBundle.getLong(EditVisitActivity.BUNDLE_KEY_START_TIMESTAMP);
            mEditVisitView.showSelectedDate(TimeUtils.timestampToPrettyDate(startTimestamp));
        }
    }

    @Override
    public List<User> getDoctors() {
        return mDoctors;
    }

    @Override
    public void onChangeDate() {
        final long startTimestamp = mBundle.getLong(EditVisitActivity.BUNDLE_KEY_START_TIMESTAMP);

        if (mDoctorsFreeCalendar == null) {
            long doctorId = mBundle.getLong(EditVisitActivity.BUNDLE_KEY_DOCTOR_ID);
            mVisitsService.getDoctorCalendar(new CalendarFilter(doctorId, TimeUtils.now(), TimeUtils.xMonthsLater(6)),
                    new VisitsServiceIf.GetDoctorCalendarCallbackIf() {
                        @Override
                        public void onGetDoctorCalendarSuccess(Map<Date, List<CalendarEntity>> doctorCalendar) {
                            Map<Date, List<CalendarEntity>> freeCalendar = AddVisitUtils.findFreeCalendarEntity(doctorCalendar);
                            mDoctorsFreeCalendar = freeCalendar;

                            if (mEditVisitView.isActive()) {
                                mEditVisitView.showDatePickerDialogWithSelectableDays(
                                        AddVisitUtils.convertDateCollectionToCalendarList(
                                                freeCalendar.keySet()), startTimestamp);
                            }
                        }

                        @Override
                        public void onGetDoctorCalendarFailure() {
                            //TODO
                        }
                    });
        } else {
            mEditVisitView.showDatePickerDialogWithSelectableDays(
                    AddVisitUtils.convertDateCollectionToCalendarList(
                            mDoctorsFreeCalendar.keySet()), startTimestamp);
        }
    }

    @Override
    public void onDateSelect(int year, int monthOfYear, int dayOfMonth) {
        mTmpSelectedDate = new GregorianCalendar(year, monthOfYear, dayOfMonth);

        List<Timepoint> selectableTimes = new ArrayList<>();
        for (CalendarEntity calendarEntity : mDoctorsFreeCalendar.get(mTmpSelectedDate.getTime())) {
            selectableTimes.add(TimeUtils.parseTime24Hours(calendarEntity.getTimeStart()));
        }

        long startTimestamp = mBundle.getLong(EditVisitActivity.BUNDLE_KEY_START_TIMESTAMP);
        mEditVisitView.showTimePickerDialogWithSelectableTimes(selectableTimes, startTimestamp);
    }

    @Override
    public void onTimeSelect(int hourOfDay, int minute) {
        mTmpSelectedDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mTmpSelectedDate.set(Calendar.MINUTE, minute);
        mSelectedDate = mTmpSelectedDate;

        DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.SHORT);
        formatter.setTimeZone(mSelectedDate.getTimeZone());
        mEditVisitView.showSelectedDate(formatter.format(mSelectedDate.getTime()));
    }

    @Override
    public void onConfirmButtonClick() {
        if (mSelectedDate == null) {
            mEditVisitView.showNoDateSelectedError();
            return;
        }

        long visitId = mBundle.getLong(EditVisitActivity.BUNDLE_KEY_VISIT_ID);
        mVisitsService.changeVisitDate(new ChangeDateRequestBody(visitId, mSelectedDate.getTimeInMillis()),
                new VisitsServiceIf.ChangeVisitDateCallbackIf() {
                    @Override
                    public void onChangeVisitDateSuccess(SimpleVisit simpleVisit) {
                        if (mEditVisitView.isActive()) {
                            mEditVisitView.showChangeVisitDateSuccess();
                        }
                    }

                    @Override
                    public void onChangeVisitDateFailure() {
                        if (mEditVisitView.isActive()) {
                            mEditVisitView.showChangeVisitDateError();
                        }
                    }
                });
    }

    private void loadDoctor(final long doctorId) {
        mUsersService.getUsers(new UserFilter(UserSearchType.SEARCH_TYPE_STAFF, 0),
                new UsersServiceIf.GetUsersCallbackIf() {
                    @Override
                    public void onGetUsersSuccess(UsersResponse usersResponse) {
                        for (User doctor : findDoctors(usersResponse.getUsers())) {
                            if (doctor.getEntityId() == doctorId) {
                                mDoctors.add(doctor);
                                break;
                            }
                        }
                        if (mEditVisitView.isActive()) {
                            mEditVisitView.refreshDoctor();
                        }
                    }

                    @Override
                    public void onGetUsersFailure() {
                        if (mEditVisitView.isActive()) {
                            mEditVisitView.showLoadingDoctorError();
                        }
                    }
                });
    }

    private List<User> findDoctors(List<User> users) {
        List<User> doctors = new ArrayList<>();
        for (User user : users) {
            if (user.getUserType() == UserType.DOCTOR) {
                doctors.add(user);
            }
        }
        return doctors;
    }
}
