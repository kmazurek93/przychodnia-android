package edu.wmi.dpri.przychodnia.android.model.data.user;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import edu.wmi.dpri.przychodnia.android.model.UserType;

public class User {

    @SerializedName("id")
    private long mId;

    @SerializedName("entityType")
    private UserType mUserType;

    @SerializedName("entityId")
    private long mEntityId;

    @SerializedName("userId")
    private long mUserId;

    @SerializedName("name")
    private String mName;

    @SerializedName("pesel")
    private String mPesel;

    @SerializedName("email")
    private String mEmail;

    @SerializedName("telephone")
    private String mTelephone;

    @SerializedName("address")
    private String mAddress;

    @Nullable
    @SerializedName("mailingAddress")
    private String mMailingAddress;

    private User() {
    }

    @Override
    public String toString() {
        return mName;
    }

    public long getId() {
        return mId;
    }

    public UserType getUserType() {
        return mUserType;
    }

    public long getEntityId() {
        return mEntityId;
    }

    public long getUserId() {
        return mUserId;
    }

    public String getName() {
        return mName;
    }

    public String getPesel() {
        return mPesel;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getTelephone() {
        return mTelephone;
    }

    public String getAddress() {
        return mAddress;
    }

    @Nullable
    public String getMailingAddress() {
        return mMailingAddress;
    }
}
