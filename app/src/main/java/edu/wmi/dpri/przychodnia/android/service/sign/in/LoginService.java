package edu.wmi.dpri.przychodnia.android.service.sign.in;

import android.support.annotation.NonNull;
import android.util.Base64;

import java.nio.charset.StandardCharsets;

import javax.inject.Inject;
import javax.inject.Singleton;

import edu.wmi.dpri.przychodnia.android.endpoint.SecurityEndpointIf;
import edu.wmi.dpri.przychodnia.android.model.data.auth.AuthResponse;
import edu.wmi.dpri.przychodnia.android.model.data.auth.LoginData;
import edu.wmi.dpri.przychodnia.android.service.auth.AuthRefreshToken;
import edu.wmi.dpri.przychodnia.android.service.auth.AuthToken;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

@Singleton
public class LoginService implements LoginServiceIf {

    private final SecurityEndpointIf mSecurityEndpoint;
    private final AuthToken mAuthToken;
    private final AuthRefreshToken mAuthRefreshToken;

    @Inject
    LoginService(@NonNull SecurityEndpointIf securityEndpoint, @NonNull AuthToken authToken,
                 @NonNull AuthRefreshToken authRefreshToken) {
        mSecurityEndpoint = checkNotNull(securityEndpoint, "securityEndpoint can not be null");
        mAuthToken = checkNotNull(authToken, "authToken can not be null");
        mAuthRefreshToken = checkNotNull(authRefreshToken, "authRefreshToken can not be null");
    }

    @Override
    public void signIn(@NonNull LoginData loginData, @NonNull final SignInCallbackIf callback) {
        checkNotNull(loginData, "loginData can not be null");
        checkNotNull(callback, "callback can not be null");

        mSecurityEndpoint.signIn(encodePasswordToBase64(loginData)).enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                onSignInResponse(response, callback);
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                onSignInFailure(t, callback);
            }
        });
    }

    private void onSignInResponse(Response<AuthResponse> response, SignInCallbackIf callback) {
        if (response.isSuccessful()) {
            AuthResponse authResponse = response.body();

            mAuthToken.setNewToken(authResponse.getToken());
            mAuthRefreshToken.setNewRefreshToken(authResponse.getRefreshToken());

            callback.onSignInSuccess();
        } else {
            callback.onSignInFailure();
        }
    }

    private void onSignInFailure(Throwable t, SignInCallbackIf callback) {
        callback.onSignInFailure();
    }

    private LoginData encodePasswordToBase64(LoginData loginData) {
        byte[] passwordBytes = loginData.getPassword().getBytes(StandardCharsets.UTF_8);
        String encodedPassword = Base64.encodeToString(passwordBytes, Base64.NO_WRAP);

        return new LoginData(loginData.getUsername(), encodedPassword);
    }
}
