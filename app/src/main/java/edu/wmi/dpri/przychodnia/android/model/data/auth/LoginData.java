package edu.wmi.dpri.przychodnia.android.model.data.auth;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static com.google.common.base.Preconditions.checkNotNull;

public class LoginData {

    @SerializedName("username")
    private String mUsername;

    @SerializedName("password")
    private String mPassword;

    public LoginData(@NonNull String username, @NonNull String password) {
        mUsername = checkNotNull(username, "username can not be null");
        mPassword = checkNotNull(password, "password can not be null");
    }

    public String getUsername() {
        return mUsername;
    }

    public String getPassword() {
        return mPassword;
    }
}
