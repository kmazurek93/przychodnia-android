package edu.wmi.dpri.przychodnia.android.edit.visit;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.util.Calendar;
import java.util.List;

import edu.wmi.dpri.przychodnia.android.BasePresenterIf;
import edu.wmi.dpri.przychodnia.android.BaseViewIf;
import edu.wmi.dpri.przychodnia.android.model.data.user.User;

public interface EditVisitContractIf {

    interface ViewIf extends BaseViewIf<PresenterIf> {

        void refreshDoctor();

        void showDatePickerDialogWithSelectableDays(List<Calendar> selectableDays, long startTimestamp);

        void showTimePickerDialogWithSelectableTimes(List<Timepoint> selectableTimes, long startTimestamp);

        void showSelectedDate(String selectedDate);

        void showChangeVisitDateSuccess();

        void showChangeVisitDateError();

        void showLoadingDoctorError();

        void showNoDoctorSelectedError();

        void showNoDateSelectedError();

        boolean isActive();
    }

    interface PresenterIf extends BasePresenterIf {

        List<User> getDoctors();

        void onChangeDate();

        void onDateSelect(int year, int monthOfYear, int dayOfMonth);

        void onTimeSelect(int hourOfDay, int minute);

        void onConfirmButtonClick();
    }
}
