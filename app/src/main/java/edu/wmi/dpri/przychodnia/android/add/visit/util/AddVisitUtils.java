package edu.wmi.dpri.przychodnia.android.add.visit.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.wmi.dpri.przychodnia.android.model.data.calendar.CalendarEntity;
import edu.wmi.dpri.przychodnia.android.model.data.calendar.CalendarEntityStatus;

public class AddVisitUtils {

    public static Map<Date, List<CalendarEntity>> findFreeCalendarEntity(
            Map<Date, List<CalendarEntity>> doctorCalendar) {
        Map<Date, List<CalendarEntity>> doctorOnlyFreeCalendar = new HashMap<>();
        for (Date date : doctorCalendar.keySet()) {
            for (CalendarEntity calendarEntity : doctorCalendar.get(date)) {
                if (calendarEntity.getStatus() == CalendarEntityStatus.FREE) {
                    if (!doctorOnlyFreeCalendar.containsKey(date)) {
                        doctorOnlyFreeCalendar.put(date, new ArrayList<CalendarEntity>());
                    }
                    doctorOnlyFreeCalendar.get(date).add(calendarEntity);
                }
            }
        }
        return doctorOnlyFreeCalendar;
    }

    public static List<Calendar> convertDateCollectionToCalendarList(Collection<Date> dates) {
        List<Calendar> calendars = new ArrayList<>();
        for (Date date : dates) {
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            calendars.add(calendar);
        }
        return calendars;
    }
}
