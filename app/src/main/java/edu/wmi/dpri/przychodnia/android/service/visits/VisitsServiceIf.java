package edu.wmi.dpri.przychodnia.android.service.visits;

import android.support.annotation.NonNull;

import java.util.Date;
import java.util.List;
import java.util.Map;

import edu.wmi.dpri.przychodnia.android.model.data.calendar.CalendarEntity;
import edu.wmi.dpri.przychodnia.android.model.data.calendar.CalendarFilter;
import edu.wmi.dpri.przychodnia.android.model.data.visit.ChangeDateRequestBody;
import edu.wmi.dpri.przychodnia.android.model.data.visit.CreateVisitRequestBody;
import edu.wmi.dpri.przychodnia.android.model.data.visit.SimpleVisit;
import edu.wmi.dpri.przychodnia.android.model.data.visit.SimpleVisitsResponse;
import edu.wmi.dpri.przychodnia.android.model.data.visit.VisitDetails;
import edu.wmi.dpri.przychodnia.android.model.data.visit.VisitFilter;

public interface VisitsServiceIf {

    interface GetSimpleVisitsCallbackIf {

        void onGetSimpleVisitsSuccess(SimpleVisitsResponse simpleVisitsResponse);

        void onGetSimpleVisitsFailure();
    }

    void getSimpleVisits(@NonNull VisitFilter visitFilter, @NonNull GetSimpleVisitsCallbackIf callback);


    interface GetDoctorCalendarCallbackIf {

        void onGetDoctorCalendarSuccess(Map<Date, List<CalendarEntity>> doctorCalendar);

        void onGetDoctorCalendarFailure();
    }

    void getDoctorCalendar(@NonNull CalendarFilter calendarFilter, @NonNull GetDoctorCalendarCallbackIf callback);


    interface CreateVisitCallbackIf {

        void onCreateVisitSuccess(SimpleVisit simpleVisit);

        void onCreateVisitFailure();
    }

    void createVisit(@NonNull CreateVisitRequestBody createVisitRequestBody, @NonNull CreateVisitCallbackIf callback);


    interface ChangeVisitDateCallbackIf {

        void onChangeVisitDateSuccess(SimpleVisit simpleVisit);

        void onChangeVisitDateFailure();
    }

    void changeVisitDate(@NonNull ChangeDateRequestBody changeDateRequestBody, @NonNull ChangeVisitDateCallbackIf callback);


    interface DeleteVisitCallbackIf {

        void onDeleteVisitSuccess();

        void onDeleteVisitFailure();
    }

    void deleteVisit(long visitId, @NonNull DeleteVisitCallbackIf callback);


    interface GetVisitDetailsCallbackIf {

        void onGetVisitDetailsSuccess(VisitDetails visitDetails);

        void onGetVisitDetailsFailure();
    }

    void getVisitDetails(long visitId, @NonNull GetVisitDetailsCallbackIf callback);
}
