package edu.wmi.dpri.przychodnia.android.add.visit;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import edu.wmi.dpri.przychodnia.android.R;
import edu.wmi.dpri.przychodnia.android.databinding.AddVisitFragBinding;
import edu.wmi.dpri.przychodnia.android.model.data.user.User;

import static com.google.common.base.Preconditions.checkNotNull;

public class AddVisitFragment extends Fragment implements AddVisitContractIf.ViewIf {

    private AddVisitContractIf.PresenterIf mAddVisitPresenter;

    private ArrayAdapter<User> mDoctorAdapter;
    private ArrayAdapter<String> mSelectedDateAdapter;

    private Spinner mDoctorSpinner;
    private Spinner mSelectedDateSpinner;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.add_visit_frag, container, false);
        AddVisitFragBinding binding = AddVisitFragBinding.bind(root);
        mDoctorSpinner = binding.addVisitFragDoctorSpinner;
        mSelectedDateSpinner = binding.addVisitFragSelectedDateSpinner;

        initDoctorAdapter();
        initSelectedDateAdapter();
        initDoctorSpinner();
        initSelectedDateSpinner();
        initConfirmButton(binding.addVisitFragConfirmButton);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mAddVisitPresenter.start();
    }

    @Override
    public void refreshAllDoctors() {
        mDoctorAdapter.notifyDataSetChanged();
    }

    @Override
    public void showDatePickerDialogWithSelectableDays(List<Calendar> selectableDays) {
        DatePickerDialog datePickerDialog = initDatePickerDialog();
        datePickerDialog.setSelectableDays(selectableDays.toArray(new Calendar[selectableDays.size()]));
        datePickerDialog.show(getFragmentManager(), "DatePickerDialog");
    }

    @Override
    public void showTimePickerDialogWithSelectableTimes(List<Timepoint> selectableTimes) {
        TimePickerDialog timePickerDialog = initTimePickerDialog();
        timePickerDialog.setSelectableTimes(selectableTimes.toArray(new Timepoint[selectableTimes.size()]));
        timePickerDialog.show(getFragmentManager(), "TimePickerDialog");
    }

    @Override
    public void showSelectedDate(String selectedDate) {
        mSelectedDateAdapter.clear();
        mSelectedDateAdapter.add(selectedDate);
        mSelectedDateAdapter.notifyDataSetChanged();
    }

    @Override
    public void showCreateVisitSuccess() {
        Toast.makeText(getActivity(), R.string.addVisit_showCreateVisitSuccess_message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showCreateVisitError() {
        //TODO
    }

    @Override
    public void showLoadingDoctorsError() {
        //TODO
    }

    @Override
    public void showNoDoctorSelectedError() {
        //TODO
    }

    @Override
    public void showNoDateSelectedError() {
        //TODO
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void showPreviousView() {
        getActivity().onBackPressed();
    }

    @Override
    public void setPresenter(@NonNull AddVisitContractIf.PresenterIf presenter) {
        mAddVisitPresenter = checkNotNull(presenter, "presenter can not be null");
    }

    private void initDoctorAdapter() {
        mDoctorAdapter = new ArrayAdapter<>(getActivity(),
                R.layout.add_visit_spinner_item, mAddVisitPresenter.getDoctors());
    }

    private void initSelectedDateAdapter() {
        List<String> list = new ArrayList<>();
        list.add("");
        mSelectedDateAdapter = new ArrayAdapter<>(getActivity(),
                R.layout.add_visit_spinner_item, list);
    }

    private void initDoctorSpinner() {
        mDoctorSpinner.setAdapter(mDoctorAdapter);
        mDoctorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mAddVisitPresenter.onDoctorSelected(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //TODO
            }
        });
    }

    private void initSelectedDateSpinner() {
        mSelectedDateSpinner.setAdapter(mSelectedDateAdapter);
        mSelectedDateSpinner.setClickable(false);
        mSelectedDateSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    mAddVisitPresenter.onDoctorSelected(mDoctorSpinner.getSelectedItemPosition());
                }
                return true;
            }
        });
    }

    private void initConfirmButton(Button confirmButton) {
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAddVisitPresenter.onConfirmButtonClick();
            }
        });
    }

    private DatePickerDialog initDatePickerDialog() {
        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                mAddVisitPresenter.onDateSelect(year, monthOfYear, dayOfMonth);
            }
        };
        Calendar now = Calendar.getInstance();
        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                onDateSetListener,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.setVersion(DatePickerDialog.Version.VERSION_2);

        return datePickerDialog;
    }

    private TimePickerDialog initTimePickerDialog() {
        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                mAddVisitPresenter.onTimeSelect(hourOfDay, minute);
            }
        };
        Calendar now = Calendar.getInstance();
        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(onTimeSetListener,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true
        );
        timePickerDialog.setVersion(TimePickerDialog.Version.VERSION_2);

        return timePickerDialog;
    }
}
