package edu.wmi.dpri.przychodnia.android.model.data.calendar;

public enum CalendarEntityType {
    DAY,
    TIME_WINDOW
}
