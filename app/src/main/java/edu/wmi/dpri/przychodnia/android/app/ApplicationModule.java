package edu.wmi.dpri.przychodnia.android.app;

import android.content.Context;
import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;

import static com.google.common.base.Preconditions.checkNotNull;

@Module
public final class ApplicationModule {

    private final Context mContext;

    ApplicationModule(@NonNull Context context) {
        mContext = checkNotNull(context, "context can not be null");
    }

    @Provides
    Context provideContext() {
        return mContext;
    }
}
