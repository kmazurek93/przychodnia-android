package edu.wmi.dpri.przychodnia.android.edit.visit;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import edu.wmi.dpri.przychodnia.android.R;
import edu.wmi.dpri.przychodnia.android.databinding.EditVisitFragBinding;
import edu.wmi.dpri.przychodnia.android.model.data.user.User;

import static com.google.common.base.Preconditions.checkNotNull;

public class EditVisitFragment extends Fragment implements EditVisitContractIf.ViewIf {

    private EditVisitContractIf.PresenterIf mEditVisitPresenter;

    private ArrayAdapter<User> mDoctorAdapter;
    private ArrayAdapter<String> mSelectedDateAdapter;

    private Spinner mDoctorSpinner;
    private Spinner mSelectedDateSpinner;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.edit_visit_frag, container, false);
        EditVisitFragBinding binding = EditVisitFragBinding.bind(root);
        mDoctorSpinner = binding.editVisitFragDoctorSpinner;
        mSelectedDateSpinner = binding.editVisitFragSelectedDateSpinner;

        initDoctorAdapter();
        initSelectedDateAdapter();
        initDoctorSpinner();
        initSelectedDateSpinner();
        initConfirmButton(binding.editVisitFragConfirmButton);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mEditVisitPresenter.start();
    }

    @Override
    public void refreshDoctor() {
        mDoctorAdapter.notifyDataSetChanged();
    }

    @Override
    public void showDatePickerDialogWithSelectableDays(List<Calendar> selectableDays, long startTimestamp) {
        DatePickerDialog datePickerDialog = initDatePickerDialog(startTimestamp);
        datePickerDialog.setSelectableDays(selectableDays.toArray(new Calendar[selectableDays.size()]));
        datePickerDialog.show(getFragmentManager(), "DatePickerDialog");
    }

    @Override
    public void showTimePickerDialogWithSelectableTimes(List<Timepoint> selectableTimes, long startTimestamp) {
        TimePickerDialog timePickerDialog = initTimePickerDialog(startTimestamp);
        timePickerDialog.setSelectableTimes(selectableTimes.toArray(new Timepoint[selectableTimes.size()]));
        timePickerDialog.show(getFragmentManager(), "TimePickerDialog");
    }

    @Override
    public void showSelectedDate(String selectedDate) {
        mSelectedDateAdapter.clear();
        mSelectedDateAdapter.add(selectedDate);
        mSelectedDateAdapter.notifyDataSetChanged();
    }

    @Override
    public void showChangeVisitDateSuccess() {

    }

    @Override
    public void showChangeVisitDateError() {

    }

    @Override
    public void showLoadingDoctorError() {

    }

    @Override
    public void showNoDoctorSelectedError() {

    }

    @Override
    public void showNoDateSelectedError() {
        Toast.makeText(getActivity(), R.string.editVisit_error_doesNotChangeDate, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(@NonNull EditVisitContractIf.PresenterIf presenter) {
        mEditVisitPresenter = checkNotNull(presenter, "presenter can not be null");
    }

    private void initDoctorAdapter() {
        mDoctorAdapter = new ArrayAdapter<>(getActivity(),
                R.layout.edit_visit_spinner_item, mEditVisitPresenter.getDoctors());
    }

    private void initSelectedDateAdapter() {
        List<String> list = new ArrayList<>();
        list.add("");
        mSelectedDateAdapter = new ArrayAdapter<>(getActivity(),
                R.layout.edit_visit_spinner_item, list);
    }

    private void initDoctorSpinner() {
        mDoctorSpinner.setAdapter(mDoctorAdapter);
        mDoctorSpinner.setClickable(false);
        mDoctorSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // do nothing
                return true;
            }
        });
    }

    private void initSelectedDateSpinner() {
        mSelectedDateSpinner.setAdapter(mSelectedDateAdapter);
        mSelectedDateSpinner.setClickable(false);
        mSelectedDateSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    mEditVisitPresenter.onChangeDate();
                }
                return true;
            }
        });
    }

    private void initConfirmButton(Button confirmButton) {
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditVisitPresenter.onConfirmButtonClick();
            }
        });
    }

    private DatePickerDialog initDatePickerDialog(long startTimestamp) {
        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                mEditVisitPresenter.onDateSelect(year, monthOfYear, dayOfMonth);
            }
        };
        Calendar startDate = new GregorianCalendar();
        startDate.setTimeInMillis(startTimestamp);
        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                onDateSetListener,
                startDate.get(Calendar.YEAR),
                startDate.get(Calendar.MONTH),
                startDate.get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.setVersion(DatePickerDialog.Version.VERSION_2);

        return datePickerDialog;
    }

    private TimePickerDialog initTimePickerDialog(long startTimestamp) {
        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                mEditVisitPresenter.onTimeSelect(hourOfDay, minute);
            }
        };
        Calendar startDate = new GregorianCalendar();
        startDate.setTimeInMillis(startTimestamp);
        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(onTimeSetListener,
                startDate.get(Calendar.HOUR_OF_DAY),
                startDate.get(Calendar.MINUTE),
                true
        );
        timePickerDialog.setVersion(TimePickerDialog.Version.VERSION_2);

        return timePickerDialog;
    }
}
