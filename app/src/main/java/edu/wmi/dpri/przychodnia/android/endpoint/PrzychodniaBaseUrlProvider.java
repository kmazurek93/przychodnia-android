package edu.wmi.dpri.przychodnia.android.endpoint;

public class PrzychodniaBaseUrlProvider {

    public static final String BASE_URL = "https://przychodnia.me/rest/";

    private PrzychodniaBaseUrlProvider() {
    }
}
