package edu.wmi.dpri.przychodnia.android.edit.user.data;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import edu.wmi.dpri.przychodnia.android.R;
import edu.wmi.dpri.przychodnia.android.util.ActivityUtils;

public class EditUserDataActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_user_data_act);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.general_appName);
            actionBar.setSubtitle(R.string.editUserData_title);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        EditUserDataFragment editUserDataFragment = new EditUserDataFragment();
        editUserDataFragment.setPresenter(new EditUserDataPresenter(editUserDataFragment));
        ActivityUtils.addFragmentToActivity(getFragmentManager(), editUserDataFragment,
                R.id.editUserDataAct_frameLayout);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
