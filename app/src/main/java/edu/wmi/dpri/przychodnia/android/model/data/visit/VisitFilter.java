package edu.wmi.dpri.przychodnia.android.model.data.visit;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import edu.wmi.dpri.przychodnia.android.model.UserType;

import static com.google.common.base.Preconditions.checkNotNull;

public class VisitFilter {

    private final static int DEFAULT_PAGE_SIZE = 20;

    @SerializedName("status")
    private VisitStatus mVisitStatus;

    @SerializedName("type")
    private UserType mUserType;

    @SerializedName("from")
    private long mFromTimestamp;

    @SerializedName("page")
    private int mPage;

    @SerializedName("size")
    private int mPageSize;

    public VisitFilter(@NonNull VisitStatus visitStatus, long fromTimestamp, int page) {
        mVisitStatus = checkNotNull(visitStatus, "visitStatus can not be null");
        mFromTimestamp = fromTimestamp;
        mUserType = UserType.PATIENT;
        mPage = page;
        mPageSize = DEFAULT_PAGE_SIZE;
    }

    public void increasePage() {
        mPage += 1;
    }

    public VisitStatus getVisitStatus() {
        return mVisitStatus;
    }

    public UserType getUserType() {
        return mUserType;
    }

    public long getFromTimestamp() {
        return mFromTimestamp;
    }

    public int getPage() {
        return mPage;
    }

    public void setPage(int page) {
        mPage = page;
    }

    public int getPageSize() {
        return mPageSize;
    }

    public void setPageSize(int pageSize) {
        mPageSize = pageSize;
    }
}
