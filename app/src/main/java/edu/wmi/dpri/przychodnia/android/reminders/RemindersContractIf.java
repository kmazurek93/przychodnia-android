package edu.wmi.dpri.przychodnia.android.reminders;

import edu.wmi.dpri.przychodnia.android.BasePresenterIf;
import edu.wmi.dpri.przychodnia.android.BaseViewIf;

interface RemindersContractIf {

    interface ViewIf extends BaseViewIf<PresenterIf> {
    }

    interface PresenterIf extends BasePresenterIf {
    }
}
