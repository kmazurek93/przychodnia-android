package edu.wmi.dpri.przychodnia.android.main;

import dagger.Component;
import edu.wmi.dpri.przychodnia.android.FragmentScoped;
import edu.wmi.dpri.przychodnia.android.service.ServicesComponent;

@FragmentScoped
@Component(dependencies = ServicesComponent.class, modules = SignOutPresenterModule.class)
interface MainComponent {

    SignOutPresenter makeSignOutPresenter();
}
