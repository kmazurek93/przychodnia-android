package edu.wmi.dpri.przychodnia.android.service;

import javax.inject.Singleton;

import dagger.Component;
import edu.wmi.dpri.przychodnia.android.app.ApplicationModule;
import edu.wmi.dpri.przychodnia.android.service.auth.AuthTokenModule;
import edu.wmi.dpri.przychodnia.android.service.sign.in.LoginService;
import edu.wmi.dpri.przychodnia.android.service.sign.out.SignOutService;
import edu.wmi.dpri.przychodnia.android.service.user.UsersService;
import edu.wmi.dpri.przychodnia.android.service.visits.VisitsService;

@Singleton
@Component(modules = {ApplicationModule.class, NetModule.class, EndpointsModule.class, AuthTokenModule.class})
public interface ServicesComponent {

    LoginService getLoginService();

    SignOutService getSignOutService();

    VisitsService getVisitsService();

    UsersService getUsersService();
}
