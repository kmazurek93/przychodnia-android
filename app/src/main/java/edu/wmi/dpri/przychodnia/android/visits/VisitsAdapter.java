package edu.wmi.dpri.przychodnia.android.visits;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import edu.wmi.dpri.przychodnia.android.R;
import edu.wmi.dpri.przychodnia.android.databinding.VisitsItemBinding;
import edu.wmi.dpri.przychodnia.android.model.data.visit.SimpleVisit;
import edu.wmi.dpri.przychodnia.android.util.TimeUtils;
import edu.wmi.dpri.przychodnia.android.util.VisitsUtils;

import static com.google.common.base.Preconditions.checkNotNull;

class VisitsAdapter extends RecyclerView.Adapter<VisitsAdapter.ViewHolder> {

    private final VisitsContractIf.PresenterIf mVisitsPresenter;

    private OnItemClickListenerIf mOnItemClickListener;
    private OnItemLongClickListenerIf mOnItemLongClickListener;
    private View.OnCreateContextMenuListener mOnCreateContextMenuListener;

    VisitsAdapter(@NonNull VisitsContractIf.PresenterIf visitsPresenter) {
        mVisitsPresenter = checkNotNull(visitsPresenter, "visitsPresenter can not be null");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View itemView = inflater.inflate(R.layout.visits_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SimpleVisit simpleVisit = mVisitsPresenter.getSimpleVisit(position);

        holder.mDateTextView.setText(TimeUtils.timestampToPrettyDate(simpleVisit.getStartTimestamp()));
        holder.mStatusTextView.setText(VisitsUtils.prettyVisitStatus(simpleVisit.getVisitStatus()));
    }

    @Override
    public int getItemCount() {
        return mVisitsPresenter.getVisitsCount();
    }


    interface OnItemClickListenerIf {
        void onItemClick(View itemView, int position);
    }

    void setOnItemClickListener(@NonNull OnItemClickListenerIf listener) {
        mOnItemClickListener = checkNotNull(listener, "listener can not be null");
    }

    interface OnItemLongClickListenerIf {
        void onItemLongClick(View itemView, int position);
    }

    void setOnItemLongClickListener(@NonNull OnItemLongClickListenerIf listener) {
        mOnItemLongClickListener = checkNotNull(listener, "listener can not be null");
    }

    void setOnCreateContextMenuListener(@NonNull View.OnCreateContextMenuListener listener) {
        mOnCreateContextMenuListener = checkNotNull(listener, "listener can not be null");
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        TextView mDateTextView;
        TextView mStatusTextView;

        ViewHolder(final View itemView) {
            super(itemView);

            VisitsItemBinding binding = VisitsItemBinding.bind(itemView);
            mDateTextView = binding.visitsItemDateTextView;
            mStatusTextView = binding.visitsItemStatusTextView;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mOnItemClickListener.onItemClick(itemView, position);
                        }
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (mOnItemLongClickListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mOnItemLongClickListener.onItemLongClick(itemView, position);
                        }
                    }
                    return false;
                }
            });
            itemView.setOnCreateContextMenuListener(mOnCreateContextMenuListener);
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.setHeaderTitle(R.string.visits_menu_context_title);
            contextMenu.add(Menu.NONE, R.id.menu_visits_context_details, 0, R.string.visits_menu_context_details);
            contextMenu.add(Menu.NONE, R.id.menu_visits_context_edit, 1, R.string.visits_menu_context_edit);
            contextMenu.add(Menu.NONE, R.id.menu_visits_context_cancel, 2, R.string.visits_menu_context_cancel);
        }
    }
}
