package edu.wmi.dpri.przychodnia.android.model.data.visit;

import com.google.gson.annotations.SerializedName;

public class ChangeDateRequestBody {

    @SerializedName("visitId")
    private long mVisitId;
    @SerializedName("newStartDate")
    private long mNewStartDateTimestamp;

    public ChangeDateRequestBody(long visitId, long newStartDateTimestamp) {
        mVisitId = visitId;
        mNewStartDateTimestamp = newStartDateTimestamp;
    }

    public long getVisitId() {
        return mVisitId;
    }

    public long getNewStartDateTimestamp() {
        return mNewStartDateTimestamp;
    }
}
