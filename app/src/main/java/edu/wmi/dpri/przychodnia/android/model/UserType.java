package edu.wmi.dpri.przychodnia.android.model;

public enum UserType {

    DOCTOR,
    EMPLOYEE,
    PATIENT,
    USER
}
