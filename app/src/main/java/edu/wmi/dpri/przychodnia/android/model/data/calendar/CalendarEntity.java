package edu.wmi.dpri.przychodnia.android.model.data.calendar;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class CalendarEntity {

    @Nullable
    @SerializedName("visitId")
    private Long mVisitId;

    @SerializedName("timeStart")
    private String mTimeStart;

    @SerializedName("timeEnd")
    private String mTimeEnd;

    @SerializedName("date")
    private Date mDate;

    @SerializedName("status")
    private CalendarEntityStatus mStatus;

    @SerializedName("type")
    private CalendarEntityType mType;

    @SerializedName("order")
    private int mOrderOnDay;

    private CalendarEntity() {
    }

    @Nullable
    public Long getVisitId() {
        return mVisitId;
    }

    public String getTimeStart() {
        return mTimeStart;
    }

    public String getTimeEnd() {
        return mTimeEnd;
    }

    public Date getDate() {
        return mDate;
    }

    public CalendarEntityStatus getStatus() {
        return mStatus;
    }

    public CalendarEntityType getType() {
        return mType;
    }

    public int getOrderOnDay() {
        return mOrderOnDay;
    }
}
