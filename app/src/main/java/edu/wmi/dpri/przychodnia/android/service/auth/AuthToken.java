package edu.wmi.dpri.przychodnia.android.service.auth;

import android.support.annotation.Nullable;

public class AuthToken {

    @Nullable
    private String mToken;

    @Nullable
    public String getToken() {
        return mToken;
    }

    public void setNewToken(@Nullable String newToken) {
        mToken = newToken;
    }

    public void invalidate() {
        mToken = null;
    }
}
