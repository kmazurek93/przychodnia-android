package edu.wmi.dpri.przychodnia.android.login;

import dagger.Component;
import edu.wmi.dpri.przychodnia.android.FragmentScoped;
import edu.wmi.dpri.przychodnia.android.service.ServicesComponent;

@FragmentScoped
@Component(dependencies = ServicesComponent.class, modules = LoginPresenterModule.class)
interface LoginComponent {

    LoginPresenter makeLoginPresenter();
}
