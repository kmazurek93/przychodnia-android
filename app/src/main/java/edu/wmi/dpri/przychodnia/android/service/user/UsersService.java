package edu.wmi.dpri.przychodnia.android.service.user;

import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Singleton;

import edu.wmi.dpri.przychodnia.android.endpoint.UsersEndpointIf;
import edu.wmi.dpri.przychodnia.android.model.data.user.UserContext;
import edu.wmi.dpri.przychodnia.android.model.data.user.UserFilter;
import edu.wmi.dpri.przychodnia.android.model.data.user.UsersResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

@Singleton
public class UsersService implements UsersServiceIf {

    private final UsersEndpointIf mUsersEndpoint;

    @Inject
    UsersService(@NonNull UsersEndpointIf usersEndpoint) {
        mUsersEndpoint = checkNotNull(usersEndpoint, "usersEndpoint can not be null");
    }

    @Override
    public void getUsers(@NonNull UserFilter userFilter, @NonNull final GetUsersCallbackIf callback) {
        checkNotNull(userFilter, "userFilter can not be null");
        checkNotNull(callback, "callback can not be null");

        mUsersEndpoint.findUsers(userFilter).enqueue(new Callback<UsersResponse>() {
            @Override
            public void onResponse(Call<UsersResponse> call, Response<UsersResponse> response) {
                onFindUsersResponse(response, callback);
            }

            @Override
            public void onFailure(Call<UsersResponse> call, Throwable t) {
                callback.onGetUsersFailure();
            }
        });
    }

    @Override
    public void getUserContext(@NonNull final GetUserContextCallbackIf callback) {
        checkNotNull(callback, "callback can not be null");

        mUsersEndpoint.getUserContext().enqueue(new Callback<UserContext>() {
            @Override
            public void onResponse(Call<UserContext> call, Response<UserContext> response) {
                onGetUserContextResponse(response, callback);
            }

            @Override
            public void onFailure(Call<UserContext> call, Throwable t) {
                callback.onGetUserContextFailure();
            }
        });
    }

    private void onFindUsersResponse(Response<UsersResponse> response, GetUsersCallbackIf callback) {
        if (response.isSuccessful()) {
            callback.onGetUsersSuccess(response.body());
        } else {
            callback.onGetUsersFailure();
        }
    }

    private void onGetUserContextResponse(Response<UserContext> response, GetUserContextCallbackIf callback) {
        if (response.isSuccessful()) {
            callback.onGetUserContextSuccess(response.body());
        } else {
            callback.onGetUserContextFailure();
        }
    }
}
